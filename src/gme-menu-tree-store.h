/*
 *  Copyright (C) 2004, 2005 Free Software Foundation, Inc.
 *
 *  Authors: Christian Neumair <chris@gnome-de.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id$
 */

#ifndef GME_MENU_TREE_STORE_H
#define GME_MENU_TREE_STORE_H

#include <gtk/gtktreestore.h>

#define GME_TYPE_MENU_TREE_STORE            (gme_menu_tree_store_get_type ())
#define GME_MENU_TREE_STORE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_MENU_TREE_STORE, GMEMenuTreeStore))
#define GME_MENU_TREE_STORE_CLASS(class)    (G_TYPE_CHECK_CLASS_CAST ((class),  GME_TYPE_MENU_TREE_STORE, GMEMenuTreeStoreClass))
#define GME_IS_MENU_TREE_STORE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_MENU_TREE_STORE))
#define GME_IS_MENU_TREE_STORE_CLASS(class) (G_TYPE_CHECK_CLASS_TYPE ((class),  GME_TYPE_MENU_TREE_STORE))
#define GME_MENU_TREE_STORE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GME_TYPE_MENU_TREE_STORE, GMEMenuTreeStoreClass))

typedef struct _GMEMenuTreeStore      GMEMenuTreeStore;
typedef struct _GMEMenuTreeStoreClass GMEMenuTreeStoreClass;

enum
{
	GME_MENU_TREE_STORE_COL_MENU,
	GME_MENU_TREE_STORE_COL_MENU_NAME,
	GME_MENU_TREE_STORE_COL_MENU_PIXBUF,
	GME_MENU_TREE_STORE_COL_MENU_IS_EMPTY,
	GME_MENU_TREE_STORE_N_COLUMNS
};

struct _GMEMenuTreeStore {
	GtkTreeStore parent;
};

struct _GMEMenuTreeStoreClass {
	GtkTreeStoreClass parent_class;
};


GtkTreeStore *gme_menu_tree_store_new (void);

#endif /* GME_MENU_TREE_STORE_H */
