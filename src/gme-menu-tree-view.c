/*
 *  Copyright (C) 2004, 2005 Free Software Foundation, Inc.
 *
 *  Authors: Christian Neumair <chris@gnome-de.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id$
 */

#include "gme-menu-tree-view.h"

#include "gme-entry-dialog.h"
#include "gme-menu-tree-store.h"
#include "gme-util.h"

#include <glib-object.h>
#include <glib/gi18n.h>

#include <gtk/gtk.h>

#define MENU_I_KNOW_THIS_IS_UNSTABLE
#include <gmenu-tree.h>

G_DEFINE_TYPE (GMEMenuTreeView, gme_menu_tree_view, GTK_TYPE_TREE_VIEW);

/* Constructors */
static void gme_menu_tree_view_init       (GMEMenuTreeView      *view);
static void gme_menu_tree_view_class_init (GMEMenuTreeViewClass *class);

/* Inherited from GObject */
static void gme_menu_tree_view_get_property (GObject      *object,
					     guint         prop_id,
					     GValue       *value,
					     GParamSpec   *pspec);
static void gme_menu_tree_view_set_property (GObject      *object,
					     guint         prop_id,
					     const GValue *value,
					     GParamSpec   *pspec);

/* Inherited from GtkWidget */
static gboolean gme_menu_tree_view_button_press       (GtkWidget      *widget,
						       GdkEventButton *event);
static gboolean gme_menu_tree_view_popup_menu         (GtkWidget      *widget);

static gboolean gme_menu_tree_view_drag_drop          (GtkWidget      *widget,
						       GdkDragContext *context,
						       gint            x,
						       gint            y,
						       guint           time);
static gboolean gme_menu_tree_view_drag_motion        (GtkWidget      *widget,
						       GdkDragContext *context,
						       int             x,
						       int             y,
						       guint           time);
static void     gme_menu_tree_view_drag_data_received (GtkWidget        *widget,
						       GdkDragContext   *context,
						       gint              x,
						       gint              y,
						       GtkSelectionData *selection,
						       guint             info,
						       guint             time);

/* Action callbacks we expose to the dialog UI Manager */
static void new_submenu_cb        (GtkAction *action, GMEMenuTreeView *treeview);
static void menu_properties_cb    (GtkAction *action, GMEMenuTreeView *treeview);

enum {
	PROP_0,
	PROP_UI_MANAGER
};

#define GME_MENU_TREE_VIEW_GET_PRIVATE(o) \
	(G_TYPE_INSTANCE_GET_PRIVATE ((o), GME_TYPE_MENU_TREE_VIEW, GMEMenuTreeViewPrivate))

typedef struct _GMEMenuTreeViewPrivate GMEMenuTreeViewPrivate;

struct _GMEMenuTreeViewPrivate {
	GtkActionGroup *action_group;
	GtkUIManager   *ui_manager;
};

static GtkTargetEntry drop_targets[] = {
	"x-internal/application", 0, 0
};

static GtkActionEntry action_entries[] = {
	{ "NewMenu",          GTK_STOCK_NEW,        N_("_New Submenu..."), NULL, NULL, G_CALLBACK (new_submenu_cb) },
	{ "MenuProperties",   GTK_STOCK_PROPERTIES, NULL,                  NULL, NULL, G_CALLBACK (menu_properties_cb) },
};

GMenuTreeDirectory *selected_menu = NULL;

static void
new_submenu_cb (GtkAction       *action,
		GMEMenuTreeView *treeview)
{
	GtkWidget  *toplevel;
	GtkWidget  *menu_dialog;
	int         response;
	const char *parent_menu_name;

	toplevel = gtk_widget_get_toplevel (GTK_WIDGET (treeview));
	parent_menu_name = gmenu_tree_directory_get_name (selected_menu);

	menu_dialog = gme_entry_dialog_new (GTK_WINDOW (toplevel),
					    NULL, ENTRY_DIALOG_MODE_NEW_SUBMENU);
	gme_entry_dialog_set_in (GME_ENTRY_DIALOG (menu_dialog), parent_menu_name);
	while ((response = gtk_dialog_run (GTK_DIALOG (menu_dialog))) == GTK_RESPONSE_HELP)
		;
	if (response == GTK_RESPONSE_OK)
		menu_file_add_menu (gme_entry_dialog_get_name (GME_ENTRY_DIALOG (menu_dialog)),
				    selected_menu);

	gtk_widget_destroy (menu_dialog);
}

static void
menu_properties_cb (GtkAction *action, GMEMenuTreeView *treeview)
{
}

static void
show_menu_popup (GMEMenuTreeView *view,
		 GdkEventButton  *event)
{
	GMEMenuTreeViewPrivate *priv;

	GtkWidget *menu;
	int        event_button;
	int        event_time;

	priv = GME_MENU_TREE_VIEW_GET_PRIVATE (view);

	if (event) {
		event_button = event->button;
		event_time = event->time;
	}
	else {
		event_button = 0;
		event_time = gtk_get_current_event_time();
	}

	menu = gtk_ui_manager_get_widget (priv->ui_manager, "/MenuPopup");
	gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL,
			event_button, event_time);
}

static gboolean
gme_menu_tree_view_button_press (GtkWidget      *widget,
				 GdkEventButton *event)
{
	GtkTreeSelection *selection;
	gboolean          ret;

	ret = GTK_WIDGET_CLASS (gme_menu_tree_view_parent_class)->button_press_event (widget, event);

	if (event->button == 3) {
		show_menu_popup (GME_MENU_TREE_VIEW (widget), event);

		ret = TRUE;
	}

	return ret;
}

static gboolean
gme_menu_tree_view_popup_menu (GtkWidget *widget)
{
	show_menu_popup (GME_MENU_TREE_VIEW (widget), NULL);

	return TRUE;
}

/* refresh information on selected menus */
static void
selection_changed (GtkTreeSelection *selection)
{
	GtkTreeModel *model;
	GtkTreeIter   iter;

	if (gtk_tree_selection_get_selected (selection, &model, &iter))
		gtk_tree_model_get (model, &iter,
				    GME_MENU_TREE_STORE_COL_MENU, &selected_menu,
				    -1);
	else
		selected_menu = NULL;

}

static gboolean
gme_menu_tree_view_drag_motion (GtkWidget      *widget,
				GdkDragContext *context,
				int             x,
				int             y,
				guint           time)
{
	GtkTargetEntry *target;
	GtkTreePath    *path;

	target = matching_context_target (context, drop_targets, G_N_ELEMENTS (drop_targets));
	if (!target) {
		gdk_drag_status (context, 0, time);

		return TRUE;
	}

	/* only allow drags on existing menus */
	if (!gtk_tree_view_get_dest_row_at_pos (GTK_TREE_VIEW (widget),
						x, y, &path, NULL)) {
		gtk_tree_view_set_drag_dest_row (GTK_TREE_VIEW (widget), NULL, 0);

		gdk_drag_status (context, 0, time);

		return TRUE;
	}

	g_object_set_data_full (G_OBJECT (context),
				"drag-motion-path", path,
				(GDestroyNotify) gtk_tree_path_free);

	gtk_drag_get_data (widget, context, gdk_atom_intern (target->target, FALSE), time);

	return TRUE;
}
		      

static void
gme_menu_tree_view_drag_data_received (GtkWidget        *widget,
				       GdkDragContext   *context,
				       gint              x,
				       gint              y,
				       GtkSelectionData *selection,
				       guint             info,
				       guint             time)
{
	GtkTreeModel *model;
	GtkTreePath  *motion_path;
	GtkTreePath  *dest_path;
	GtkTreeIter   iter;

	GMenuTreeEntry     *entry;
	GMenuTreeDirectory *entry_parent;
	GMenuTreeDirectory *target;

	g_return_if_fail (selection);
	g_return_if_fail (selection->length >= 0);
	g_return_if_fail (matching_context_target (context, drop_targets, G_N_ELEMENTS (drop_targets)));

	memcpy (&entry, selection->data, sizeof (entry));
	g_return_if_fail (entry);

	g_object_set_data (G_OBJECT (context), "drop-entry", entry);

	entry_parent = gmenu_tree_item_get_parent (GMENU_TREE_ITEM (entry));
	g_return_if_fail (entry_parent);

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (widget));

	motion_path = g_object_get_data (G_OBJECT (context), "drag-motion-path");

	gtk_tree_model_get_iter (model, &iter, motion_path);
	gtk_tree_model_get (model, &iter,
			    GME_MENU_TREE_STORE_COL_MENU, &target,
			    -1);

	/* drag-motion feedback: only allow drops to unselected rows */
	if (target == selected_menu) {
		context->action = 0;
		gtk_tree_view_set_drag_dest_row (GTK_TREE_VIEW (widget), NULL, 0);
		gdk_drag_status (context, 0, time);

		return;
	}

	context->action = context->suggested_action;
	gdk_drag_status (context, context->suggested_action, time);
	gtk_tree_view_set_drag_dest_row (GTK_TREE_VIEW (widget), motion_path,
					 GTK_TREE_VIEW_DROP_INTO_OR_BEFORE);
}

static gboolean
gme_menu_tree_view_drag_drop (GtkWidget      *widget,
			      GdkDragContext *context,
			      gint            x,
			      gint            y,
			      guint           time)
{
	GtkTreeModel *model;
	GtkTreePath  *motion_path;
	GtkTreePath  *dest_path;
	GtkTreeIter   iter;

	GMenuTreeEntry     *entry;
	GMenuTreeDirectory *target;

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (widget));

	motion_path = g_object_get_data (G_OBJECT (context), "drag-motion-path");
	g_return_val_if_fail (motion_path, FALSE);

	entry = g_object_steal_data (G_OBJECT (context), "drop-entry");
	g_return_val_if_fail (entry, FALSE);

	gtk_tree_model_get_iter (model, &iter, motion_path);
	gtk_tree_model_get (model, &iter,
			    GME_MENU_TREE_STORE_COL_MENU, &target,
			    -1);

	menu_file_copy_entry (entry, target);

	g_object_set_data (G_OBJECT (context), "drag-motion-path", NULL);

	gtk_drag_finish (context, TRUE, FALSE, time);

	return TRUE;
}

static void
gme_menu_tree_view_get_property (GObject      *object,
				 guint         prop_id,
				 GValue       *value,
				 GParamSpec   *pspec)
{
	GMEMenuTreeViewPrivate *priv;

	priv = GME_MENU_TREE_VIEW_GET_PRIVATE (object);

	switch (prop_id) {
		case PROP_UI_MANAGER:
			g_value_set_object (value, priv->ui_manager);
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
setup_action_group (GMEMenuTreeView *view)
{
	GMEMenuTreeViewPrivate *priv;

	priv = GME_MENU_TREE_VIEW_GET_PRIVATE (view);

	priv->action_group = gtk_action_group_new ("MenuGroup");
	gtk_action_group_set_translation_domain (priv->action_group, NULL);
	gtk_action_group_add_actions (priv->action_group, action_entries,
				      G_N_ELEMENTS (action_entries), view);
}

static void
gme_menu_tree_view_set_property (GObject      *object,
				 guint         prop_id,
				 const GValue *value,
				 GParamSpec   *pspec)
{
	GMEMenuTreeViewPrivate *priv;

	priv = GME_MENU_TREE_VIEW_GET_PRIVATE (object);

	switch (prop_id) {
		case PROP_UI_MANAGER:
			if (GTK_IS_UI_MANAGER (priv->ui_manager) &&
			    GTK_IS_ACTION_GROUP (priv->action_group))
				gtk_ui_manager_remove_action_group (priv->ui_manager, priv->action_group);

			priv->ui_manager = GTK_UI_MANAGER (g_value_get_object (value));
			setup_action_group (GME_MENU_TREE_VIEW (object));
			gtk_ui_manager_insert_action_group (priv->ui_manager, priv->action_group, 0);
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}

}

static void
gme_menu_tree_view_init (GMEMenuTreeView *view)
{
	GtkCellRenderer   *renderer;
	GtkTreeViewColumn *column;
	GtkTreeSelection  *selection;

	column = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (column, _("Name"));
	gtk_tree_view_column_set_spacing (column, 6);
	gtk_tree_view_append_column (GTK_TREE_VIEW (view), column);

	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (column, renderer, FALSE);
	gtk_tree_view_column_add_attribute (column, renderer,
					    "pixbuf", GME_MENU_TREE_STORE_COL_MENU_PIXBUF);

	renderer = gtk_cell_renderer_text_new ();
	g_object_set (G_OBJECT (renderer), "style", PANGO_STYLE_OBLIQUE, NULL);
	gtk_tree_view_column_pack_end (column, renderer, TRUE);
	gtk_tree_view_column_set_attributes (column, renderer,
					     "text", GME_MENU_TREE_STORE_COL_MENU_NAME,
					     "style-set", GME_MENU_TREE_STORE_COL_MENU_IS_EMPTY,
					     NULL);

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (view));
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_BROWSE);

	g_signal_connect (selection, "changed",
			  G_CALLBACK (selection_changed),
			  NULL);

	/* drag and drop */
	gtk_tree_view_enable_model_drag_dest (GTK_TREE_VIEW (view), 
					      drop_targets, G_N_ELEMENTS (drop_targets),
					      GDK_ACTION_COPY);
}

static void
gme_menu_tree_view_class_init (GMEMenuTreeViewClass *class)
{
	GObjectClass   *object_class;
	GtkWidgetClass *widget_class;

	object_class = G_OBJECT_CLASS (class);
	object_class->get_property = gme_menu_tree_view_get_property;
	object_class->set_property = gme_menu_tree_view_set_property;

	widget_class = GTK_WIDGET_CLASS (class);
	widget_class->button_press_event = gme_menu_tree_view_button_press;
	widget_class->popup_menu = gme_menu_tree_view_popup_menu;

	/* D'n'D */
	widget_class->drag_drop = gme_menu_tree_view_drag_drop;
	widget_class->drag_motion = gme_menu_tree_view_drag_motion;
	widget_class->drag_data_received = gme_menu_tree_view_drag_data_received;

	g_object_class_install_property (object_class, PROP_UI_MANAGER,
					 g_param_spec_object ("ui-manager", "UI Manager", NULL,
						 	      GTK_TYPE_UI_MANAGER,
							      G_PARAM_CONSTRUCT_ONLY |
							      G_PARAM_READWRITE));

	g_type_class_add_private (class, sizeof (GMEMenuTreeViewPrivate));
}

GtkWidget *
gme_menu_tree_view_new (GtkUIManager *ui_manager)
{
  return g_object_new (GME_TYPE_MENU_TREE_VIEW,
		       "model", gme_menu_tree_store_new (),
		       "ui-manager", ui_manager,
		       NULL);
}
