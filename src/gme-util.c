/*
 *  Copyright (C) 2004, 2005 Free Software Foundation, Inc.
 *
 *  Authors: Christian Neumair <chris@gnome-de.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id$
 */

#include "gme-menu-tree-store.h"
#include "gme-application-list-store.h"

#include <glib/gi18n.h>
#include <glib/gprintf.h>

#include <gtk/gtk.h>

#include <libxml/globals.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

#define GMENU_I_KNOW_THIS_IS_UNSTABLE
#include <gmenu-tree.h>

#define PIXBUF_SIZE 24

static void  model_add_recursive (GMenuTreeDirectory       *dir,
				  GMEMenuTreeStore        *menu_tree_store,
				  GtkTreeIter             *menu_parent_iter,
				  GMEApplicationListStore *app_list_store,
				  gboolean                *added_entries);

static xmlDocPtr menu_target_file_to_doc (void);
static void      doc_to_menu_target_file (xmlDocPtr doc);

static GHashTable *icon_pixbufs = NULL;

static char   *user_app_basepath;
static GSList *system_app_basepaths;

static char *user_menu_basepath;
static char *user_menu_path;
static char *user_old_menu_path;

typedef enum {
	ERROR_CREATING_DIRECTORY,
	ERROR_NO_ROOT_DIRECTORY,
	ERROR_PARSING_ISSUE
} Error;

typedef struct {
	const Error  error;

	const char  *primary_message;
	const char  *primary_arg;

	const char  *secondary_message;
	const char  *secondary_args[2]; /* We have to use the max. number of arguments for this */
} ErrorToMessage;

ErrorToMessage error_to_message[] = {
	{ ERROR_CREATING_DIRECTORY,

	  N_("Could Not Create Directory %s"), NULL,

	  N_("The menu editor was unable to create the directory %s, which is necessary for storing "
	     "menu modifications. Changes to the menu structure will not take effect."), NULL
	},

	{ ERROR_NO_ROOT_DIRECTORY,

	  N_("Could Not Open Application Root Directory"), NULL,

	  N_("The menu editor was unable to open your application root directory. "
	     "Most likely your %s environment variable wasn't set correctly.\n"
	     "Contact your system administrator or distributor for information on what "
	     "particular paths the %s environment variable should contain."),
	     { "<i>$XDG_CONFIG_DIRS</i>", "<i>$XDG_CONFIG_DIRS</i>" } }
};

/* find out whether the Error error has a message associated with it.
 * Optionally return it through the etm parameter */
static gboolean
lookup_message_for_error (Error            error,
			  ErrorToMessage **etm)
{
	int i;

	for (i = 0; i < G_N_ELEMENTS (error_to_message); i++)
		if (error == error_to_message[i].error) {
			if (etm)
				*etm = &error_to_message[i];

			return TRUE;
		}

	return FALSE;
}

/* display a stock error dialog with an optional argument */
static void
display_error_dialog (Error  error,
		      char  *arg)
{
	ErrorToMessage *etm;
	GtkWidget      *dialog;

	g_return_if_fail (lookup_message_for_error (error, &etm));

	dialog = gtk_message_dialog_new (NULL, 0,
					 GTK_MESSAGE_WARNING,
					 GTK_BUTTONS_CLOSE,
					 _(etm->primary_message),
					 arg ? arg :etm->primary_arg);
	gtk_message_dialog_format_secondary_markup (GTK_MESSAGE_DIALOG (dialog),
						    _(etm->secondary_message),
						    arg ? arg : etm->secondary_args ? etm->secondary_args[0] : NULL,
						    arg ? arg : etm->secondary_args && etm->secondary_args[1] ? etm->secondary_args[1] : NULL);
	gtk_window_set_title (GTK_WINDOW (dialog), "");

	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
}

/* create a new desktop key file */
static GKeyFile *
desktop_key_file_new (const char *name,
		      const char *exec,
		      const char *comment)
{
	GKeyFile *ret;

	g_return_val_if_fail (name, NULL);
	g_return_val_if_fail (exec, NULL);

	ret = g_key_file_new ();

	g_key_file_set_string (ret, "Desktop Entry", "Encoding", "UTF-8");
	g_key_file_set_string (ret, "Desktop Entry", "Type", "Application");
	g_key_file_set_string (ret, "Desktop Entry", "Name", name);
	g_key_file_set_string (ret, "Desktop Entry", "Exec", exec);
	g_key_file_set_string (ret, "Desktop Entry", "Comment", comment);

	return ret;
}

static gboolean
path_exists (const char *basepath,
	     const char *suffix)
{
	char    *path;
	gboolean  ret;

	path = g_build_filename (basepath, suffix, NULL);
	ret = g_file_test (path, G_FILE_TEST_EXISTS);

	g_free (path);

	return ret;
}

/* checks whether the file suffix is contained
 * in at least one of base_paths. suffix may be NULL */
static gboolean
composed_path_exists (GSList      *basepaths,
		      const char  *suffix)
{
	GSList *l;

	g_return_if_fail (basepaths);

	for (l = basepaths; l; l = l->next)
		if (path_exists (l->data, suffix))
			return TRUE;

	return FALSE;
}

/* returns whether there exists a file with the specified file id,
 * either in one of the global or in the user-specific application directory */
static gboolean
desktop_file_with_id_exists (char *desktop_file_id)
{
	if (composed_path_exists (system_app_basepaths, desktop_file_id))
		return TRUE;

	if (path_exists (user_app_basepath, desktop_file_id))
		return TRUE;

	return FALSE;
}

/* counts the numbers a string ends in */
static unsigned int
count_suffix_digits (const char *string)
{
	int len, i;

	g_return_val_if_fail (string, 0);

	len = strlen (string);
	if (len < 1)
		return FALSE;

	for (i = len; i > 0; i--)
		if (string[i-1] < '0' ||
		    string[i-1] > '9')
			break;

	return len - i;
}

/* ensures that a filename relative to the XDG/applications dirs is unused.
 * If it is already used, it is changed to an unused relative filename. */
static void
ensure_desktop_file_id_is_unused (char **desktop_file_id)
{
	char *p, *tmp;

	int i;

	g_return_if_fail (desktop_file_id);

	if (!desktop_file_with_id_exists (*desktop_file_id))
		return;

	/* truncate extension */
	p = g_strrstr (*desktop_file_id, ".desktop");
	*p = '\0';

	switch (count_suffix_digits (*desktop_file_id)) {
		case 0:
			break;

		/* truncate any number triple ending, they were most likely
		 * added by the loop below */
		case 3:
			*(p-3) = '\0';
			break;

		/* if the string ends in more or less digits than 3 ("evolution-2.0"), we
		 * * put a "-" in the end, so that the copies are called "evolution-2.0-001" etc. */
		default:
			/* No need to realloc, since we truncated ".desktop" */
			*p = '-';
			*(p+1) = '\0';
			break;
	}

	/* for "epiphany", this will try to use "epiphany001", "epiphany002", ...,
	 * and uses the first one that doesn't exist yet */
	for (i = 1; i < 1000; i++) {
		tmp = g_strdup_printf ("%s%03d.desktop", *desktop_file_id, i);

		if (!desktop_file_with_id_exists (tmp)) {
			g_free (*desktop_file_id);
			*desktop_file_id = tmp;

			break;
		}

		g_free (tmp);
	}

	/* will we EVER have to deal with more than 999 attempts to add the same desktop file */
	g_assert (i < 1000);
}

/* returns a filename calculated from the name information which is
 * used as relative filename (to XDG_DATA_DIRS elements.
 * TODO check whether this works correctly for all keyfiles */
static char *
find_unused_file_id (const char *name)
{
	char *ret, *s;

	g_return_val_if_fail (name, NULL);

	ret = g_utf8_strdown (name, -1);

	/* ensure that our user-specific entries don't clash with system-wide
	 * applications that are not yet installed/developed. */
	s = g_strconcat ("custom-", ret, ".desktop", NULL);
	g_free (ret);
	ret = s;

	/* replace all spaces and slashes by dashes */
	for (s = ret; s && *s != '\0'; s++)
		if (*s == ' ' || *s == '/')
			*s = '-';

	/* ensure that no other file exists or change filename */
	ensure_desktop_file_id_is_unused (&ret);

	return ret;
}


/* ensure that the local path path exists on disk */
static gboolean
ensure_path_exists (char *path)
{
	int i;

	g_return_val_if_fail (path, FALSE);

	/* OK, path already exists. We're done. */
	if (g_file_test (path, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_DIR))
		return TRUE;

	/* create all path components that don't exist yet. Ignore first character ('/') */
	for (i = 1; path[i] != '\0' ; i++) {
		gboolean is_last_run;

		switch (path[i]) {
			case '/':
				path[i] = '\0';
				is_last_run = FALSE;
				break;

			case '\0':
				is_last_run = TRUE;
				break;

			default:
				continue;
				break;
		}

		/* path already exists */
		if (g_file_test (path, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_DIR)) {
			if (!is_last_run)
				path[i] = '/';

			continue;
		}

		if (!mkdir (path, 0755)) {
			if (!is_last_run)
				path[i] = '/';

			continue; /* successfully created path */
		}

		/* we failed. Eeek. Yell. */
		display_error_dialog (ERROR_CREATING_DIRECTORY, path);

		if (!is_last_run)
			path[i] = '/';

		return FALSE;
	}

	return TRUE;
}

/* returns whether the new applications.menu file (gnome-menu-editor > 0.2)
 * or the old one (gnome-menu-editor <= 0.2) exist. */
gboolean
menu_files_exist (void)
{
	return (g_file_test (user_menu_path, G_FILE_TEST_EXISTS) ||
	        g_file_test (user_old_menu_path, G_FILE_TEST_EXISTS));
}

/* create a new node with the <Name> name below an existing <Menu> node
 * which has parent_node as parent */
static xmlNodePtr
create_menu_node (const char   *name,
		  xmlNodePtr    parent_node)
{
	xmlNodePtr node;
	xmlNodePtr child_node;

	g_return_val_if_fail (name, NULL);

	/* add blanks & line-breaks (for aesthetical reasons) */ 

	if (parent_node)
		node = xmlNewChild (parent_node, NULL, "Menu", NULL);
	else
		node = xmlNewNode (NULL, "Menu");

	child_node = xmlNewChild (node, NULL, "Name", NULL);
	xmlNodeSetContent (child_node, name);

	return node;
}

/* returns the first child node of node
 * which has the name name or NULL if
 * none exists with the name name. */
static xmlNodePtr
get_child_node_with_name (const char  *name,
			  xmlNodePtr   parent_node)
{
	xmlNodePtr ret;

	g_return_val_if_fail (name, NULL);
	g_return_val_if_fail (parent_node, NULL);

	for (ret = parent_node->children; ret; ret = ret->next) {
		if (!ret->name ||
		    strcmp (ret->name, name))
			continue;

		break;
	}

	return ret;
}

static xmlNodePtr
get_or_create_child_node_with_name (const char  *name,
				    xmlNodePtr   parent_node)
{
	xmlNodePtr ret;

	g_return_val_if_fail (name, NULL);
	g_return_val_if_fail (parent_node, NULL);

	if (ret = get_child_node_with_name (name, parent_node))
		return ret;

	return xmlNewChild (parent_node, NULL, name, NULL);
}

/* try to find an existing <Menu> node with the <Name> name
 * which has parent_node as parent */
static xmlNodePtr
get_child_menu_node (const char  *name,
		     xmlNodePtr   parent_node)
{
	xmlNodePtr node;
	xmlNodePtr name_node;

	g_return_val_if_fail (name, NULL);
	g_return_val_if_fail (parent_node, NULL);

	for (node = parent_node->children; node; node = node->next) {
		char *tmp;

		if (strcmp (node->name, "Menu"))
			continue;

		name_node = get_child_node_with_name ("Name", node);

		/* check whether it is the right submenu */
		if (!name_node)
			continue;

		tmp = xmlNodeGetContent (name_node);

		if (strcmp (tmp, name)) {
			g_free (tmp);
			continue;
		}

		/* ok, we found it */
		g_free (tmp);

		break;
	}

	return node;
}

/* get a menu node which is a child of parent node and has the name
 * name if it exists; otherwise create it. */
static xmlNodePtr
get_or_create_menu_node (const char  *name,
			 xmlNodePtr   parent_node)
{
	xmlNodePtr child_node;

	g_return_val_if_fail (name, NULL);
	g_return_val_if_fail (parent_node, NULL);

	child_node = get_child_menu_node (name, parent_node);

	/* node exists, return it */
	if (child_node)
		return child_node;

	/* we've got to create it */
	return create_menu_node (name, parent_node);
}

/* creates a Layout node below parent_node that contains
 * a Menuname node for the menu named name with "show_empty=true",
 * so that it will be shown even if it doesn't contain anything */
static xmlNodePtr
get_or_create_layout_child_node_for_menu (const char  *name,
					  xmlNodePtr   parent_node)
{
	xmlNodePtr node;
	xmlNodePtr child_node;

	g_return_val_if_fail (name, NULL);
	g_return_val_if_fail (parent_node, NULL);

	node = get_or_create_child_node_with_name ("Layout", parent_node);

	/* find matching Menuname node */
	for (child_node = node->children; child_node; child_node = child_node->next) {
		char *tmp;

		if (!child_node->name)
			continue;

		if (strcmp (child_node->name, "Menuname"))
			continue;

		tmp = xmlNodeGetContent (child_node);
		if (!tmp || strcmp (tmp, name)) {
			g_free (tmp);
			continue;
		}

		break;
	}

	/* doesn't exist yet */
	if (!child_node) {
		child_node = xmlNewChild (node, NULL, "Menuname", NULL);
		xmlNodeSetContent (child_node, name);
	}

	xmlSetProp (child_node, "show_empty", "true");

	return child_node;
}

static GSList *
strs_to_slist (char **strs)
{
	int     i;
	GSList *ret;

	g_return_val_if_fail (strs, NULL);

	for (ret = NULL, i = 0; strs[i]; i++)
		if (*strs[i] != '\0')
		ret = g_slist_append (ret, g_strdup (strs[i]));

	return ret;
}

static GSList *
dir_to_path (GMenuTreeDirectory *dir)
{
	char    *path;
	char   **strs;
	GSList  *ret;

	g_return_val_if_fail (dir, NULL);

	path = gmenu_tree_directory_make_path (dir, NULL);
	strs = g_strsplit (path, "/", 0);
	ret = strs_to_slist (strs);

	g_free (path);
	g_strfreev (strs);

	return ret;
}

static xmlNodePtr
get_or_create_menu_node_for_tree_directory (GMenuTreeDirectory *dir,
					    xmlDocPtr          doc)
{
	GSList *path, *l;

	xmlNodePtr node;

	node = xmlDocGetRootElement (doc);

	if (!node) {
		node = create_menu_node ("Applications", NULL);
		xmlDocSetRootElement (doc, node);
	}

	path = dir_to_path (dir);

	for (l = path; l && l->data; l = l->next)
		node = get_or_create_menu_node (l->data, node);

	g_slist_foreach (path,
			 (GFunc) g_free,
			 NULL);
	g_slist_free (path);

	return node;
}

/* create a menu node for the specified entry. */
static xmlNodePtr
get_or_create_menu_node_for_entry (GMenuTreeEntry *entry,
				   xmlDocPtr      doc)
{
	return get_or_create_menu_node_for_tree_directory (gmenu_tree_item_get_parent (GMENU_TREE_ITEM (entry)), doc);
}

/* delete old information on the menu tree entry entry in
 * menu menu_node */
static void
delete_old_entry_information_for_menu (GMenuTreeEntry *entry,
				       xmlNodePtr     menu_node)
{
	xmlNodePtr   child_node;
	const char  *file_id;
	char        *tmp;

	GSList *nodes_to_unlink;

	g_return_if_fail (entry);
	g_return_if_fail (menu_node);

	file_id = gmenu_tree_entry_get_desktop_file_id (entry);

	nodes_to_unlink = NULL;

	for (child_node = menu_node->children; child_node; child_node = child_node->next) {
		xmlNodePtr filename_node;

		if (strcmp (child_node->name, "Include") &&
		    strcmp (child_node->name, "Exclude"))
			continue;

		filename_node = get_child_node_with_name ("Filename", child_node);

		if (!filename_node) /* shouldn't happen */
			continue;

		tmp = xmlNodeGetContent (filename_node);

		/* this is an include/exclude node referring to another desktop file id */
		if (strcmp (tmp, file_id)) {
			g_free (tmp);
			continue;
		}

		g_free (tmp);


		/* ok, we got one. We can't yet remove it, because we still need it for the loop. */
		nodes_to_unlink = g_slist_prepend (nodes_to_unlink, child_node);
	}

	/* actually unlink and free nodes marked for removal */
	g_slist_foreach (nodes_to_unlink,
			 (GFunc) xmlUnlinkNode,
			 NULL);

	g_slist_foreach (nodes_to_unlink,
			 (GFunc) xmlFreeNode,
			 NULL);

	g_slist_free (nodes_to_unlink);
}

/* function for recursively deleting all information related from entry */
static void
real_delete_old_entry_information (GMenuTreeEntry *entry,
				   xmlNodePtr     node)
{
	delete_old_entry_information_for_menu (entry, node);

	for (node = node->children; node; node = node->next) {
		if (!node->name)
			continue;

		if (!strcmp (node->name, "Menu"))
			real_delete_old_entry_information (entry, node);
	}
}

/* delete all old information on the menu tree entry */
static void
delete_old_entry_information (GMenuTreeEntry *entry)
{
	xmlDocPtr  doc;
	xmlNodePtr node;

	doc = menu_target_file_to_doc ();

	node = xmlDocGetRootElement (doc);
	if (!node)
		return;

	real_delete_old_entry_information (entry, node);

	doc_to_menu_target_file (doc);
}


/* create a node for a desktop file id below menu_node
 * which is either an include or an exclude node, based
 * on the display parameter */
static void
create_desktop_file_id_node (const char  *desktop_file_id,
			     xmlNodePtr   menu_node,
			     gboolean     display)
{
	xmlNodePtr child_node;
	xmlNodePtr filename_node;

	g_return_if_fail (desktop_file_id);
	g_return_if_fail (menu_node);

	child_node = xmlNewChild (menu_node,
				  NULL,
				  display ? "Include" : "Exclude",
				  NULL);

	filename_node = xmlNewChild (child_node,
				     NULL,
				     "Filename",
				     NULL);
	xmlNodeSetContent (filename_node, desktop_file_id);

}

/* create a node for a GMenuTreeEntry below menu_node
 * which is either an include or an exclude node, based
 * on the display parameter */
static void
create_entry_node (GMenuTreeEntry *entry,
		   xmlNodePtr     menu_node,
		   gboolean       display)
{
	const char *desktop_file_id;

	g_return_if_fail (entry);
	g_return_if_fail (menu_node);

	desktop_file_id = gmenu_tree_entry_get_desktop_file_id (entry);

	create_desktop_file_id_node (desktop_file_id, menu_node, display);
}

static xmlDocPtr
menu_target_file_to_doc (void)
{
	xmlDocPtr  doc = NULL;
	xmlNodePtr node;

	/* if the menu file exists, try to parse it */
	if (g_file_test (user_menu_path, G_FILE_TEST_EXISTS))
		doc = xmlParseFile (user_menu_path);

        /* if parsing failed or the file doesn't exist, create a new one */
	if (!doc) {
		doc = xmlNewDoc ("1.0");
		node = create_menu_node ("Applications", NULL);
		xmlDocSetRootElement (doc, node);

		/* create <MergeFile> node for merging global applications.menu */
		node = xmlNewChild (node, NULL, "MergeFile", NULL);
		xmlSetProp (node, "type", "parent");
	}

	return doc;
}

static void
doc_to_menu_target_file (xmlDocPtr doc)
{
	g_return_if_fail (ensure_path_exists (user_menu_basepath));

	xmlIndentTreeOutput = 1;
	xmlKeepBlanksDefault (0);
	xmlSaveFormatFile (user_menu_path, doc, 1);
	xmlFreeDoc (doc);

	xmlCleanupParser ();
}

static void
write_keyfile (GKeyFile    *keyfile,
	       const char  *path)
{
	GIOChannel *key_file_io_channel;
	char       *data;

	g_return_if_fail (keyfile);
	g_return_if_fail (path);

	data = g_key_file_to_data (keyfile, NULL, NULL);

	key_file_io_channel = g_io_channel_new_file (path, "w", NULL);
	g_io_channel_write_chars (key_file_io_channel, data, -1, NULL, NULL);
	g_io_channel_shutdown (key_file_io_channel, TRUE, NULL);
	g_io_channel_unref (key_file_io_channel);

	g_free (data);
}

static char *
create_desktop_key_file (const char *name,
			 const char *exec,
			 const char *comment)
{
	GKeyFile *keyfile;

	char     *id;
	char     *path;

	g_return_val_if_fail (name, NULL);
	g_return_val_if_fail (exec, NULL);

	g_return_val_if_fail (ensure_path_exists (user_app_basepath), NULL);

	keyfile = desktop_key_file_new (name, exec, comment);

	id = find_unused_file_id (name);
	path = g_build_filename (user_app_basepath, id, NULL);
	write_keyfile (keyfile, path);

	g_free (path);

	return id;
}

gboolean
menu_file_add_entry (const char        *name,
		     const char        *exec,
		     const char        *comment,
		     GMenuTreeDirectory *parent_dir)
{
	xmlDocPtr   doc;
	xmlNodePtr  node;

	char *desktop_file_id;

	g_return_val_if_fail (name, FALSE);
	g_return_val_if_fail (exec, FALSE);
	g_return_val_if_fail (parent_dir, FALSE);

	desktop_file_id = create_desktop_key_file (name, exec, comment);
	g_return_val_if_fail (desktop_file_id, FALSE);

	doc = menu_target_file_to_doc ();
	node = get_or_create_menu_node_for_tree_directory (parent_dir, doc);
	create_desktop_file_id_node (desktop_file_id, node, TRUE);

	doc_to_menu_target_file (doc);

	g_free (desktop_file_id);
}

gboolean
menu_file_add_menu (const char         *name,
		    GMenuTreeDirectory *parent_dir)
{
	xmlDocPtr   doc;
	xmlNodePtr  node;

	doc = menu_target_file_to_doc ();
	node = get_or_create_menu_node_for_tree_directory (parent_dir, doc);

	/* if it already existed, we failed */
	if (get_child_menu_node (name, node)) {

		xmlFreeDoc (doc);
		return FALSE;
	}

	create_menu_node (name, node);
	doc_to_menu_target_file (doc);

	return TRUE;
}

/* copies a file from src to dest, optionally
 * removing the "Categories" desktop entry section,
 * as well as the "MimeTypes" section. The latter
 * is useful to not mess up the MIME system with
 * launcher copies */
static void
copy_desktop_file (const char  *src,
		   const char  *dest,
		   gboolean     remove_categories,
		   gboolean     remove_mime_types)
{
	GKeyFile *keyfile;

	g_return_if_fail (src);
	g_return_if_fail (dest);

	keyfile = g_key_file_new ();
	g_key_file_load_from_file (keyfile, src,
				   G_KEY_FILE_KEEP_COMMENTS |
				   G_KEY_FILE_KEEP_TRANSLATIONS, NULL);
	g_return_if_fail (keyfile);

	if (remove_categories && g_key_file_has_key (keyfile, "Desktop Entry", "Categories", NULL))
		g_key_file_remove_key (keyfile, "Desktop Entry", "Categories", NULL);

	if (remove_mime_types && g_key_file_has_key (keyfile, "Desktop Entry", "MimeTypes", NULL))
		g_key_file_remove_key (keyfile, "Desktop Entry", "MimeTypes", NULL);

	write_keyfile (keyfile, dest);

	g_key_file_free (keyfile);
}

/* copies the specified entry to a new file.
 * The copy is given a new desktop file id, which is returned through new_desktop_id */
static void
copy_entry_to_new_file (GMenuTreeEntry  *entry,
			char           **new_desktop_id_ret)
{
	const char *id;
	char *new_path;
	char *new_desktop_id;

	g_return_if_fail (entry);

	id = gmenu_tree_entry_get_desktop_file_id (entry);

	/* if the entry id begins with id, we chop the prefix */
	new_desktop_id = g_str_has_prefix (id, "custom-")
		? g_strdup (new_desktop_id  + 7)
		: g_strdup (id);
	ensure_desktop_file_id_is_unused (&new_desktop_id);

	new_path = g_build_filename (user_app_basepath, new_desktop_id, NULL);
	copy_desktop_file (gmenu_tree_entry_get_desktop_file_path (entry), new_path, TRUE, TRUE);

	if (new_desktop_id_ret)
		*new_desktop_id_ret = new_desktop_id;
	else
		g_free (new_desktop_id);

	g_free (new_path);
}

/* updates the menu file for entry, which is copied to the target directory */
gboolean
menu_file_copy_entry (GMenuTreeEntry     *entry,
		      GMenuTreeDirectory *target)
{
	xmlDocPtr  doc;
	xmlNodePtr node;

	char *new_desktop_id;

	g_return_val_if_fail (entry, FALSE);
	g_return_val_if_fail (target, FALSE);
	g_return_val_if_fail (gmenu_tree_item_get_parent (GMENU_TREE_ITEM (entry)), FALSE);

	doc = menu_target_file_to_doc ();
	g_return_val_if_fail (doc, FALSE);

	node = get_or_create_menu_node_for_tree_directory (target, doc);
	g_return_val_if_fail (node, FALSE);

	copy_entry_to_new_file (entry, &new_desktop_id);

	create_desktop_file_id_node (new_desktop_id, node, TRUE);
	doc_to_menu_target_file (doc);

	g_free (new_desktop_id);
}

/* update the menu file for entry, which is either shown or hidden */
gboolean
menu_file_set_display_entry (GMenuTreeEntry *entry,
			     gboolean       display)
{
	xmlDocPtr  doc;
	xmlNodePtr node;

	doc = menu_target_file_to_doc ();
	g_return_val_if_fail (doc, FALSE);

	node = get_or_create_menu_node_for_entry (entry, doc);
	g_return_val_if_fail (node, FALSE);

	delete_old_entry_information_for_menu (entry, node);

	create_entry_node (entry, node, display);

	doc_to_menu_target_file (doc);

	return TRUE;
}

static void
real_remove_references_entries (xmlNodePtr node)
{
	for (node = node->children; node; node = node->next) {
		xmlNodePtr  filename_node;
		char       *path;
		char       *id;

		if (!node->name)
			continue;

		if (!strcmp (node->name, "Include") ||
		    !strcmp (node->name, "Exclude")) {
			filename_node = get_child_node_with_name ("Filename", node);

			if (!filename_node) /* shouldn't happen */
				continue;

			id = xmlNodeGetContent (filename_node);

			path = g_build_filename (user_app_basepath, id, NULL);
			if (g_file_test (path, G_FILE_TEST_EXISTS))
				remove (path);

			g_free (path);
			g_free (id);
			continue;
		}

		if (!strcmp (node->name, "Menu"))
			real_remove_references_entries (node);
	}
}

/* removes all user-owned entries that are references in the given menu file */
static void
menu_file_remove_referenced_entries (char *menu_file)
{
	xmlDocPtr  doc;
	xmlNodePtr node;

	doc = xmlParseFile (menu_file);

	node = xmlDocGetRootElement (doc);
	real_remove_references_entries (node);
}

/* delete menu files */
void
menu_files_delete (void)
{
	if (g_file_test (user_old_menu_path, G_FILE_TEST_EXISTS)) {
		menu_file_remove_referenced_entries (user_old_menu_path);
		remove (user_old_menu_path);
	}

	if (g_file_test (user_menu_path, G_FILE_TEST_EXISTS)) {
		menu_file_remove_referenced_entries (user_menu_path);
		remove (user_menu_path);
	}
}

/* truncates file extension from the given string.
 * Taken from GNOME Panel. Returns FALSE if failed. */
static gboolean
truncate_extension (char *file)
{
	char *p;

	p = strrchr (file, '.');

	if (p &&
	    (!strcmp (p, ".png") ||
	     !strcmp (p, ".xpm") ||
	     !strcmp (p, ".svg"))) {
		*p = '\0';
		return TRUE;
	}

	return FALSE;
}


/* looks up the pixbuf for a particular icon name. If only_if_exists is TRUE,
 * the pixbuf is only returned if it was previously cached already */
static GdkPixbuf *
lookup_pixbuf (const char *icon_name,
	       gboolean     only_if_exists)
{
	GtkIconTheme *icon_theme;
	GdkPixbuf    *ret;

	if (!icon_name)
		return NULL;

	/* try to get cached pixbuf first */
	ret = g_hash_table_lookup (icon_pixbufs, icon_name);

	if (ret)
		return ret;

	if (only_if_exists)
		return NULL;

	if (g_path_is_absolute (icon_name))
		ret = gdk_pixbuf_new_from_file_at_size (icon_name, PIXBUF_SIZE, PIXBUF_SIZE, NULL);
	else {
		char *basename;

		icon_theme = gtk_icon_theme_get_default ();

		basename = g_path_get_basename (icon_name);
		truncate_extension (basename);

		ret = gtk_icon_theme_load_icon (icon_theme, basename, 24, 0, NULL);

		g_free (basename);
	}

	if (ret)
		g_hash_table_insert (icon_pixbufs, (char *) icon_name, ret);

	return ret;
}

static GdkPixbuf *
lookup_directory_pixbuf (GMenuTreeDirectory *dir,
			 gboolean           only_if_exists)
{
	g_return_val_if_fail (dir, NULL);

	return lookup_pixbuf (gmenu_tree_directory_get_icon (dir), only_if_exists);
}

static GdkPixbuf *
lookup_entry_pixbuf (GMenuTreeEntry *entry,
		     gboolean       only_if_exists)
{
	g_return_val_if_fail (entry, NULL);

	return lookup_pixbuf (gmenu_tree_entry_get_icon (entry), only_if_exists);
}

/* returns TRUE if visible entries have been added to this menu or a submenu */
static gboolean
real_add_items_recursive (GMEMenuTreeStore        *menu_tree_store,
			  GtkTreeIter             *menu_iter,
			  GMEApplicationListStore *app_list_store,
			  GSList                  *items)
{
	GMenuTreeItem *item;
	GtkTreeIter    iter;

	gboolean ret = FALSE;

	g_return_if_fail (menu_tree_store);
	g_return_if_fail (app_list_store);
	g_return_if_fail (menu_iter);

	for (; items; items = items->next) {
		item = GMENU_TREE_ITEM (items->data);

		switch (gmenu_tree_item_get_type (item)) {
			case GMENU_TREE_ITEM_DIRECTORY:
				model_add_recursive (GMENU_TREE_DIRECTORY (item),
						     menu_tree_store,
						     menu_iter,
						     app_list_store,
						     ret ? NULL : &ret);
				break;

			case GMENU_TREE_ITEM_ENTRY:
				if (!ret)
					ret = !gmenu_tree_entry_get_is_excluded (GMENU_TREE_ENTRY (item));

				gtk_list_store_append (GTK_LIST_STORE (app_list_store), &iter);
				gtk_list_store_set (GTK_LIST_STORE (app_list_store), &iter,
						    GME_APPLICATION_LIST_STORE_COL_MENU_TREE_ENTRY, item,
						    GME_APPLICATION_LIST_STORE_COL_APPLICATION_NAME, gmenu_tree_entry_get_name (GMENU_TREE_ENTRY (item)),
						    GME_APPLICATION_LIST_STORE_COL_PIXBUF, lookup_entry_pixbuf (GMENU_TREE_ENTRY (item), TRUE),
						    GME_APPLICATION_LIST_STORE_COL_DISPLAY, !gmenu_tree_entry_get_is_excluded (GMENU_TREE_ENTRY (item)),
						    -1);
				break;

			default:
				break;
		}
	}

	return ret;
}

/* adds a directory to the menu/app stores and optionally returns whether
 * the menu or one of its submenus actually contain at least one visible entry */
static void
model_add_recursive (GMenuTreeDirectory       *dir,
		     GMEMenuTreeStore        *menu_tree_store,
		     GtkTreeIter             *menu_parent_iter,
		     GMEApplicationListStore *app_list_store,
		     gboolean                *has_entries)
{
	GtkTreeIter iter;
	GtkTreeIter child_iter;

	GSList *list;

	gboolean real_has_entries;

	/* Insert the directory itself */
	gtk_tree_store_append (GTK_TREE_STORE (menu_tree_store),
			       &iter,
			       menu_parent_iter);

	/* Insert displayed submenus/entries */
	list = gmenu_tree_directory_get_contents (dir);
	real_has_entries = real_add_items_recursive (menu_tree_store, &iter, app_list_store, list);
	g_slist_free (list);

	/* Fill in the directory information */
	gtk_tree_store_set (GTK_TREE_STORE (menu_tree_store), &iter,
			    GME_MENU_TREE_STORE_COL_MENU, gmenu_tree_item_ref (GMENU_TREE_ITEM (dir)),
			    GME_MENU_TREE_STORE_COL_MENU_NAME, gmenu_tree_directory_get_name (dir),
			    GME_MENU_TREE_STORE_COL_MENU_PIXBUF, lookup_directory_pixbuf (dir, TRUE),
			    GME_MENU_TREE_STORE_COL_MENU_IS_EMPTY, !real_has_entries,
			    -1);

	if (has_entries)
		*has_entries = real_has_entries;
}

static gboolean
menu_lookup_icon_foreach (GtkTreeModel *model,
			  GtkTreePath  *path,
			  GtkTreeIter  *iter)
{
	GMenuTreeDirectory *dir;

	gtk_tree_model_get (model, iter,
			    GME_MENU_TREE_STORE_COL_MENU, &dir,
			    -1);

	g_return_val_if_fail (dir, FALSE);

	gtk_tree_store_set (GTK_TREE_STORE (model), iter,
			    GME_MENU_TREE_STORE_COL_MENU_PIXBUF, lookup_directory_pixbuf (dir, FALSE),
			    -1);

	return FALSE;
}

static gboolean
app_lookup_icon_foreach (GtkTreeModel *model,
			 GtkTreePath  *path,
			 GtkTreeIter  *iter)
{
	GMenuTreeEntry *entry;

	gtk_tree_model_get (model, iter,
			    GME_APPLICATION_LIST_STORE_COL_MENU_TREE_ENTRY, &entry,
			    -1);

	g_return_val_if_fail (entry, FALSE);

	gtk_list_store_set (GTK_LIST_STORE (model), iter,
			    GME_APPLICATION_LIST_STORE_COL_PIXBUF, lookup_entry_pixbuf (entry, FALSE),
			    -1);

	return FALSE;
}

static gboolean
menu_icon_idle (GMEMenuTreeStore *menu_tree_store)
{
	gtk_tree_model_foreach (GTK_TREE_MODEL (menu_tree_store),
				(GtkTreeModelForeachFunc) menu_lookup_icon_foreach,
				NULL);

	return FALSE;
}

static gboolean
app_icon_idle (GMEApplicationListStore *app_list_store)
{
	gtk_tree_model_foreach (GTK_TREE_MODEL (app_list_store),
				(GtkTreeModelForeachFunc) app_lookup_icon_foreach,
				NULL);

	return FALSE;
}

static void
setup_icon_idle (GMEMenuTreeStore        *menu_tree_store,
		 GMEApplicationListStore *app_list_store)
{
	g_idle_add_full (G_PRIORITY_LOW,
			 (GSourceFunc) menu_icon_idle,
			 menu_tree_store, NULL);

	g_idle_add_full (G_PRIORITY_LOW,
			 (GSourceFunc) app_icon_idle,
			 app_list_store, NULL);
}

gboolean
fill_tree_views (GtkTreeView *directory_tree_view,
		 GtkTreeView *application_tree_view,
		 GMenuTree   **tree)
{
	GMEMenuTreeStore        *menu_tree_store;
	GMEApplicationListStore *app_list_store;

	GMenuTreeDirectory *root;

	g_return_val_if_fail (directory_tree_view, FALSE);
	g_return_val_if_fail (application_tree_view, FALSE);
	g_return_val_if_fail (tree, FALSE);

	menu_tree_store = GME_MENU_TREE_STORE (gtk_tree_view_get_model (directory_tree_view));
	app_list_store = GME_APPLICATION_LIST_STORE (
		gtk_tree_model_filter_get_model (
			GTK_TREE_MODEL_FILTER (
				gtk_tree_view_get_model (application_tree_view))));

	if (!*tree)
		*tree = gmenu_tree_lookup ("applications.menu",
					   GMENU_TREE_FLAGS_INCLUDE_EXCLUDED |
					   GMENU_TREE_FLAGS_SHOW_EMPTY);

	root = gmenu_tree_get_root_directory (*tree);
	if (!root) {
		display_error_dialog (ERROR_NO_ROOT_DIRECTORY, NULL);

		return FALSE;
	}

	model_add_recursive (root,
			     menu_tree_store,
			     NULL,
			     app_list_store,
			     NULL);
	setup_icon_idle (menu_tree_store,
			 app_list_store);

	gmenu_tree_item_unref (GMENU_TREE_ITEM (root));

	return TRUE;
}

static gboolean
is_user_menu_entry (GMenuTreeEntry *entry)
{
	const char *path;

	path = gmenu_tree_entry_get_desktop_file_path (entry);

	return g_str_has_prefix (path, user_app_basepath);
}

static gboolean
has_global_desktop_entry_with_same_file_id (GMenuTreeEntry *entry)
{
	const char *path;
	GSList      *l;

	path = gmenu_tree_entry_get_desktop_file_path (entry);

	for (l = system_app_basepaths; l; l = l->next)
		if (g_str_has_prefix (path, l->data))
			return TRUE;

	return FALSE;
}

/* taken from gnome-desktop/gnome-desktop-item.c */
static const char *
get_language (void)
{
	const char * const *langs_pointer;
	int                 i;

	langs_pointer = g_get_language_names ();
	for (i = 0; langs_pointer[i] != NULL; i++) {
		/* find first without encoding  */
		if (strchr (langs_pointer[i], '.') == NULL) {
			return langs_pointer[i];
		}
	}

	return NULL;
}

void
menu_entry_change (GMenuTreeEntry *entry,
		   const char    *name,
		   const char    *exec,
		   const char    *comment)
{
	GKeyFile   *keyfile;
	const char *path, *id;
	char       *out_path;
	const char *lang;

	g_return_if_fail (entry);
	g_return_if_fail (name);
	g_return_if_fail (exec);

	path = gmenu_tree_entry_get_desktop_file_path (entry);
	id = gmenu_tree_entry_get_desktop_file_id (entry);

	/* read-in the old keyfile */
	keyfile = g_key_file_new ();
	g_key_file_load_from_file (keyfile, path,
				   G_KEY_FILE_KEEP_COMMENTS |
				   G_KEY_FILE_KEEP_TRANSLATIONS,
				   NULL);

	lang = get_language ();
	if (lang) {
		g_key_file_set_locale_string (keyfile, "Desktop Entry", "Name", lang, name);
		g_key_file_set_locale_string (keyfile, "Desktop Entry", "Exec", lang, exec);
		g_key_file_set_locale_string (keyfile, "Desktop Entry", "Comment", lang, comment);
	}
	else {
		g_key_file_set_string (keyfile, "Desktop Entry", "Name", name);
		g_key_file_set_string (keyfile, "Desktop Entry", "Exec", exec);
		g_key_file_set_string (keyfile, "Desktop Entry", "Comment", comment);
	}

	out_path = g_build_filename (user_app_basepath, id, NULL);
	write_keyfile (keyfile, out_path);

/* FIXME basically this is a hack to ensure that on menu file deletion, this desktop file id is
 * included in the menu tree and therefore will be deleted as well. */
	menu_file_set_display_entry (entry, !gmenu_tree_entry_get_is_excluded (entry));

	g_key_file_free (keyfile);
	g_free (out_path);
}


gboolean
menu_entry_can_be_deleted (GMenuTreeEntry *entry)
{
	g_return_val_if_fail (entry, FALSE);

	if (!is_user_menu_entry (entry) ||
	    has_global_desktop_entry_with_same_file_id (entry))
		return FALSE;

	return TRUE;
}

void
menu_entry_delete (GMenuTreeEntry *entry)
{
	const char *path;

	g_return_if_fail (menu_entry_can_be_deleted (entry));

	path = gmenu_tree_entry_get_desktop_file_path (entry);
	remove (path);

	delete_old_entry_information (entry);

	return;
}

/* returns whether the given GdkDragContext matches any of the given targets
 * optionally returns the first matching target */
GtkTargetEntry *
matching_context_target (GdkDragContext *context,
			 GtkTargetEntry *targets,
			 unsigned int    n_targets)
{
	int      i;
	gboolean matched;

	g_return_val_if_fail (context, FALSE);

	for (i = 0, matched = FALSE; i < n_targets; i++)
		if (g_list_find (context->targets, gdk_atom_intern (targets[i].target, FALSE)))
			return &targets[i];

	return NULL;
}

/* get the base path of all system desktop files */
static GSList *
get_system_app_basepaths (void)
{
	const char * const *paths;
	GSList *ret = NULL;

	for (paths = g_get_system_data_dirs (); paths && *paths; paths++)
		ret = g_slist_append (ret, g_build_filename (*paths, "applications/", NULL));

	return ret;
}

void
gme_util_init (void)
{
	/* Paths */
	user_menu_basepath = g_build_filename (g_get_user_config_dir (), "menus/", NULL);
	user_menu_path = g_build_filename (user_menu_basepath, "applications.menu", NULL);
	user_old_menu_path = g_build_filename (user_menu_basepath, "applications-merged", "applications.menu", NULL);

	user_app_basepath = g_build_filename (g_get_user_data_dir (), "applications/", NULL);
	system_app_basepaths = get_system_app_basepaths ();

	/* Pixbuf Hash Table */
	icon_pixbufs = g_hash_table_new (g_str_hash, g_str_equal);
}

void
gme_util_cleanup (void)
{
	GSList *l;

	/* User Menu Paths */
	g_free (user_menu_basepath);
	g_free (user_menu_path);
	g_free (user_old_menu_path);

	/* Application Paths */
	g_free (user_app_basepath);

	g_slist_foreach (system_app_basepaths,
			 (GFunc) g_free,
			 NULL);
	g_slist_free (system_app_basepaths);

	/* Pixbuf Hash Table */
	g_hash_table_destroy (icon_pixbufs);
}
