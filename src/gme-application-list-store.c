/*
 *  Copyright (C) 2004, 2005 Free Software Foundation, Inc.
 *
 *  Authors: Christian Neumair <chris@gnome-de.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id$
 */

#include "gme-application-list-store.h"

#include <glib-object.h>

#include <gdk/gdkpixbuf.h>

#include <gtk/gtk.h>

#define GMENU_I_KNOW_THIS_IS_UNSTABLE
#include <gmenu-tree.h>

static void gme_application_list_store_init       (GMEApplicationListStore *view);
static void gme_application_list_store_class_init (GMEApplicationListStoreClass *class);

G_DEFINE_TYPE (GMEApplicationListStore,
	       gme_application_list_store,
	       GTK_TYPE_LIST_STORE);

static void
gme_application_list_store_init (GMEApplicationListStore *store)
{
	GType column_types[] = {
		G_TYPE_POINTER,  /* GME_APPLICATION_LIST_STORE_COL_MENU_TREE_ENTRY */
		G_TYPE_STRING,   /* GME_APPLICATION_LIST_STORE_COL_APPLICATION_NAME */
		GDK_TYPE_PIXBUF, /* GME_APPLICATION_LIST_STORE_COL_PIXBUF */
		G_TYPE_BOOLEAN   /* GME_APPLICATION_LIST_STORE_COL_DISPLAY */
	};

	gtk_list_store_set_column_types (GTK_LIST_STORE (store),
					 GME_APPLICATION_LIST_STORE_N_COLUMNS,
					 column_types);

	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (store),
					      GME_APPLICATION_LIST_STORE_COL_APPLICATION_NAME,
					      GTK_SORT_ASCENDING);
}

static void
gme_application_list_store_class_init (GMEApplicationListStoreClass *class)
{
}

GtkTreeModel *
gme_application_list_store_new (void)
{
	return g_object_new (GME_TYPE_APPLICATION_LIST_STORE,
			     NULL);
}
