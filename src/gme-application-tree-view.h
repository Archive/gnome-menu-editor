/*
 *  Copyright (C) 2004, 2005 Free Software Foundation, Inc.
 *
 *  Authors: Christian Neumair <chris@gnome-de.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id$
 */

#ifndef GME_APPLICATION_TREE_VIEW_H
#define GME_APPLICATION_TREE_VIEW_H

#include <gtk/gtk.h>

#define GME_TYPE_APPLICATION_TREE_VIEW            (gme_application_tree_view_get_type ())
#define GME_APPLICATION_TREE_VIEW(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_APPLICATION_TREE_VIEW, GMEApplicationTreeView))
#define GME_APPLICATION_TREE_VIEW_CLASS(class)    (G_TYPE_CHECK_CLASS_CAST ((class),  GME_TYPE_APPLICATION_TREE_VIEW, GMEApplicationTreeViewClass))
#define GME_IS_APPLICATION_TREE_VIEW(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_APPLICATION_TREE_VIEW))
#define GME_IS_APPLICATION_TREE_VIEW_CLASS(class) (G_TYPE_CHECK_CLASS_TYPE ((class),  GME_TYPE_APPLICATION_TREE_VIEW))
#define GME_APPLICATION_TREE_VIEW_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GME_TYPE_APPLICATION_TREE_VIEW, GMEApplicationTreeViewClass))

typedef struct _GMEApplicationTreeView      GMEApplicationTreeView;
typedef struct _GMEApplicationTreeViewClass GMEApplicationTreeViewClass;

struct _GMEApplicationTreeView {
	GtkTreeView parent;
};

struct _GMEApplicationTreeViewClass {
	GtkTreeViewClass parent_class;
};

GtkWidget *gme_application_tree_view_new (GtkTreeSelection *filter_selection,
					  GtkUIManager     *ui_manager);

#endif /* GME_APPLICATION_TREE_VIEW_H */
