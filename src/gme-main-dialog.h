/*
 *  Copyright (C) 2004, 2005 Free Software Foundation, Inc.
 *
 *  Authors: Christian Neumair <chris@gnome-de.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id$
 */

#include <gtk/gtk.h>

#ifndef GME_MAIN_DIALOG_H
#define GME_MAIN_DIALOG_H

#define GME_TYPE_MAIN_DIALOG            (gme_main_dialog_get_type ())
#define GME_MAIN_DIALOG(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_MAIN_DIALOG, GMEMainDialog))
#define GME_MAIN_DIALOG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_MAIN_DIALOG, GMEMainDialog))
#define GME_IS_MAIN_DIALOG(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_MAIN_DIALOG))
#define GME_IS_MAIN_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_MAIN_DIALOG))
#define GME_MAIN_DIALOG_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_MAIN_DIALOG, GMEMainDialog))

#define DIALOG_RESPONSE_DEFAULT 0

typedef struct _GMEMainDialog      GMEMainDialog;
typedef struct _GMEMainDialogClass GMEMainDialogClass;

struct _GMEMainDialog
{
	GtkDialog parent;
};

struct _GMEMainDialogClass
{
	GtkDialogClass parent_class;

	void (*reread_menu) (GMEMainDialog *dialog);
};

GtkWidget *gme_main_dialog_new (void);

#endif
