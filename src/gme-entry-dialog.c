/*
 *  Copyright (C) 2004, 2005 Free Software Foundation, Inc.
 *
 *  Authors: Christian Neumair <chris@gnome-de.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id$
 */

#include "gme-entry-dialog.h"

#include <glib-object.h>
#include <glib/gi18n.h>

#include <gtk/gtk.h>

#define GMENU_I_KNOW_THIS_IS_UNSTABLE
#include <gmenu-tree.h>

#define GME_ENTRY_DIALOG_GET_PRIVATE(o) \
	(G_TYPE_INSTANCE_GET_PRIVATE ((o), GME_TYPE_ENTRY_DIALOG, GMEEntryDialogPrivate))

typedef struct _GMEEntryDialogPrivate GMEEntryDialogPrivate;

struct _GMEEntryDialogPrivate
{
	/* if we used a table instead of a vbox and a sizegroup,
	 * hiding single rows would lead to a layout mess */
	GtkSizeGroup *label_size_group;

	GtkWidget *vbox;

	GtkWidget *name_hbox;
	GtkWidget *name_label;
	GtkWidget *name_entry;

	GtkWidget *comment_hbox;
	GtkWidget *comment_label;
	GtkWidget *comment_entry;

	GtkWidget *executable_hbox;
	GtkWidget *executable_label;
	GtkWidget *executable_entry;
	GtkWidget *executable_button;

	GtkWidget *in_hbox;
	GtkWidget *in_label1;
	GtkWidget *in_label2;

	GtkWidget *icon_label;
	GtkWidget *icon_button;

	GMenuTreeEntry *entry;
	/* these will be strcmp'ed against the current GtkEntry contents
	 * to find out whether they changed and the user may press O.K. */
	char           *entry_name;
	char           *entry_comment;
	char           *entry_executable;

	GMEEntryDialogMode  mode;
};

G_DEFINE_TYPE (GMEEntryDialog, gme_entry_dialog, GTK_TYPE_DIALOG);

enum {
	PROP_0,
	PROP_ENTRY,
	PROP_MODE
};

GType
gme_entry_dialog_mode_get_type (void)
{
	static GType type = 0;

	if (!type) {
		 static const GEnumValue values[] = {
			 { ENTRY_DIALOG_MODE_NEW_APPLICATION, "ENTRY_DIALOG_MODE_NEW_APPLICATION", "new-application" },
			 { ENTRY_DIALOG_MODE_EDIT_APPLICATION, "ENTRY_DIALOG_MODE_EDIT_APPLICATION", "edit-application" },
			 { ENTRY_DIALOG_MODE_NEW_SUBMENU, "ENTRY_DIALOG_MODE_NEW_SUBMENU", "new-menu" },
			 { ENTRY_DIALOG_MODE_EDIT_MENU, "ENTRY_DIALOG_MODE_EDIT_MENU", "edit-menu" }
		 };

		 type = g_enum_register_static ("EntryDialogMode", values);
	}
}

static gboolean
visible_and_empty (GtkEntry *entry)
{
	const char *text;

	g_return_val_if_fail (entry, FALSE);

	if (!GTK_WIDGET_MAPPED (GTK_WIDGET (entry)))
		return FALSE;

	text = gtk_entry_get_text (entry);
	if (!text || text[0] == '\0')
		return TRUE;

	return FALSE;
}

static gboolean
entry_matches_text (GtkEntry    *entry,
		    const char  *text)
{
	g_return_val_if_fail (entry, FALSE);

	return !g_utf8_collate (gtk_entry_get_text (entry), text ? text : "");
}

static void
entry_changed (GtkEntry       *entry,
	       GMEEntryDialog *dialog)
{
	GMEEntryDialogPrivate *priv;

	gboolean ok_sensitive;

	priv = GME_ENTRY_DIALOG_GET_PRIVATE (dialog);

	switch (priv->mode) {
		case ENTRY_DIALOG_MODE_NEW_APPLICATION:
		case ENTRY_DIALOG_MODE_NEW_SUBMENU:
			ok_sensitive = !visible_and_empty (GTK_ENTRY (priv->name_entry)) &&
				       !visible_and_empty (GTK_ENTRY (priv->executable_entry));
			break;

		case ENTRY_DIALOG_MODE_EDIT_APPLICATION:
		case ENTRY_DIALOG_MODE_EDIT_MENU:
			ok_sensitive = priv->entry &&
				       (!entry_matches_text (GTK_ENTRY (priv->name_entry), priv->entry_name) ||
				        !entry_matches_text (GTK_ENTRY (priv->comment_entry), priv->entry_comment) ||
				        !entry_matches_text (GTK_ENTRY (priv->executable_entry), priv->entry_executable));
			break;

		default:
			g_assert_not_reached ();
			break;
	}

	gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog), GTK_RESPONSE_OK, ok_sensitive);
}

static void
entry_filechooser (GtkWidget      *widget,
		   GMEEntryDialog *dialog)
{	GMEEntryDialogPrivate *priv;

	GtkWidget     *chooser;
	GtkFileFilter *chooser_filter;

	priv = GME_ENTRY_DIALOG_GET_PRIVATE (dialog);

	chooser = gtk_file_chooser_dialog_new (_("Select an Executable"),
					       GTK_WINDOW (dialog),
					       GTK_FILE_CHOOSER_ACTION_OPEN,
					       GTK_STOCK_CANCEL,
					       GTK_RESPONSE_CANCEL,
					       GTK_STOCK_OPEN,
					       GTK_RESPONSE_OK,
					       NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (chooser), GTK_RESPONSE_OK);

	chooser_filter = gtk_file_filter_new ();
	gtk_file_filter_add_mime_type (chooser_filter, "application/x-executable");
	gtk_file_filter_add_mime_type (chooser_filter, "application/x-shellscript");
	gtk_file_chooser_set_filter (GTK_FILE_CHOOSER (chooser), chooser_filter);

	if (gtk_dialog_run (GTK_DIALOG (chooser)) == GTK_RESPONSE_OK) {
		char *file;

		file = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (chooser));
		gme_entry_dialog_set_executable (dialog, file);
		g_free (file);
	}

	gtk_widget_destroy (GTK_WIDGET (chooser));

	gtk_widget_grab_focus (priv->executable_entry);
}

static void
update_from_mode (GMEEntryDialog     *dialog,
		  GMEEntryDialogMode  mode)
{
	GMEEntryDialogPrivate *priv;

	priv = GME_ENTRY_DIALOG_GET_PRIVATE (dialog);

	/* Set Title */
	switch (mode) {
		case ENTRY_DIALOG_MODE_NEW_APPLICATION:
			gtk_window_set_title (GTK_WINDOW (dialog), _("New Application"));

			break;
		case ENTRY_DIALOG_MODE_EDIT_APPLICATION:
			gtk_window_set_title (GTK_WINDOW (dialog), _("Edit Application"));

			break;
		case ENTRY_DIALOG_MODE_NEW_SUBMENU:
			gtk_window_set_title (GTK_WINDOW (dialog), _("New Submenu"));

			break;
		case ENTRY_DIALOG_MODE_EDIT_MENU:
			gtk_window_set_title (GTK_WINDOW (dialog), _("Edit Menu"));

			break;
		default:
			g_assert_not_reached ();
	}

	/* Show/hide widgets according to mode
	 *
	 * GtkSizeGroup is broken and takes hidden widgets into account when calculating the size to
	 * be allocated for all widgets in the group (#171612). Therefore, widgets have to be added/removed
	 * to/from the size group when being shown/hidden */
	switch (mode) {
		case ENTRY_DIALOG_MODE_NEW_APPLICATION:
		case ENTRY_DIALOG_MODE_EDIT_APPLICATION:
			gtk_widget_show (priv->comment_hbox);
			gtk_widget_show (priv->executable_hbox);
			gtk_widget_hide (priv->in_hbox);

			gtk_size_group_add_widget (priv->label_size_group, priv->comment_label);
			gtk_size_group_add_widget (priv->label_size_group, priv->executable_label);
			gtk_size_group_remove_widget (priv->label_size_group, priv->in_label1);
			break;

		case ENTRY_DIALOG_MODE_NEW_SUBMENU:
			gtk_widget_hide (priv->comment_hbox);
			gtk_widget_hide (priv->executable_hbox);
			gtk_widget_show (priv->in_hbox);

			gtk_size_group_remove_widget (priv->label_size_group, priv->comment_label);
			gtk_size_group_remove_widget (priv->label_size_group, priv->executable_label);
			gtk_size_group_add_widget (priv->label_size_group, priv->in_label1);
			break;

		case ENTRY_DIALOG_MODE_EDIT_MENU:
			gtk_widget_hide (priv->comment_hbox);
			gtk_widget_hide (priv->executable_hbox);
			gtk_widget_hide (priv->in_hbox);

			gtk_size_group_remove_widget (priv->label_size_group, priv->comment_label);
			gtk_size_group_remove_widget (priv->label_size_group, priv->executable_label);
			gtk_size_group_remove_widget (priv->label_size_group, priv->in_label1);
			break;

		default:
			g_assert_not_reached ();
			break;
	}

	gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog), GTK_RESPONSE_OK, FALSE);

	priv->mode = mode;
	g_object_notify (G_OBJECT (dialog), "mode");
}

static void
update_from_entry (GMEEntryDialog  *dialog,
		   GMenuTreeEntry  *entry)
{
	GMEEntryDialogPrivate *priv;
	const char *name, *comment, *exec;

	priv = GME_ENTRY_DIALOG_GET_PRIVATE (dialog);

	if (entry) {
		name = gmenu_tree_entry_get_name (entry);
		comment = gmenu_tree_entry_get_comment (entry);
		exec = gmenu_tree_entry_get_exec (entry);
	}
	else {
		name = NULL;
		comment = NULL;
		exec = NULL;
	}

	gme_entry_dialog_set_name (dialog, name);
	gme_entry_dialog_set_comment (dialog, comment);
	gme_entry_dialog_set_executable (dialog, exec);

	g_free (priv->entry_name);
	g_free (priv->entry_comment);
	g_free (priv->entry_executable);

	priv->entry = entry;
	priv->entry_name = g_strdup (gtk_entry_get_text (GTK_ENTRY (priv->name_entry)));
	priv->entry_comment = g_strdup (gtk_entry_get_text (GTK_ENTRY (priv->comment_entry)));
	priv->entry_executable = g_strdup (gtk_entry_get_text (GTK_ENTRY (priv->executable_entry)));

	g_object_notify (G_OBJECT (dialog), "entry");
}

static void
add_double_label_hbox_to_vbox (GtkVBox       *vbox,
			       const char    *text,
			       GtkSizeGroup  *label1_size_group,
			       GtkWidget    **hbox,
			       GtkWidget    **label1,
			       GtkWidget    **label2)
{
	GtkWidget *real_hbox;
	GtkWidget *real_label1;
	GtkWidget *real_label2;

	/* HBox */
	real_hbox = gtk_hbox_new (FALSE, 12);
	gtk_container_add (GTK_CONTAINER (vbox), real_hbox);

	/* First Label */
	real_label1 = gtk_label_new (text);
	gtk_misc_set_alignment (GTK_MISC (real_label1), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (real_hbox), real_label1, FALSE, TRUE, 0);

	if (label1_size_group)
		gtk_size_group_add_widget (label1_size_group, real_label1);

	/* Second Label */
	real_label2 = gtk_label_new ("I'm a bug. File me.");
	gtk_misc_set_alignment (GTK_MISC (real_label2), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (real_hbox), real_label2, FALSE, TRUE, 0);

	if (hbox)
		*hbox = real_hbox;

	if (label1)
		*label1 = real_label1;

	if (label2)
		*label2 = real_label2;
}

static void
add_entry_hbox_to_vbox (GtkVBox       *vbox,
			const char    *text,
			GtkSizeGroup  *label_size_group,
			GtkWidget    **hbox,
			GtkWidget    **label,
			GtkWidget    **entry,
			GtkWidget    **button,
			const char    *button_text)
{
	GtkWidget *real_hbox;
	GtkWidget *real_label;
	GtkWidget *real_entry;
	GtkWidget *real_button;

	GtkWidget *button_hbox;

	g_return_if_fail (vbox);
	g_return_if_fail (text);
	g_return_if_fail ((button && button_text) ||
			  (!button && !button_text));

	/* HBox */
	real_hbox = gtk_hbox_new (FALSE, 12);
	gtk_container_add (GTK_CONTAINER (vbox), real_hbox);

	/* Label */
	real_label = gtk_label_new_with_mnemonic (text);
	gtk_misc_set_alignment (GTK_MISC (real_label), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (real_hbox), real_label, FALSE, TRUE, 0);

	if (label_size_group)
		gtk_size_group_add_widget (label_size_group, real_label);

	/* Entry */
	real_entry = gtk_entry_new ();
	gtk_entry_set_activates_default (GTK_ENTRY (real_entry), TRUE);
	gtk_label_set_mnemonic_widget (GTK_LABEL (real_label), real_entry);

	if (!button_text) {
		gtk_box_pack_start (GTK_BOX (real_hbox), real_entry, TRUE, TRUE, 0);
	} else {
		/* HBox, Entry */
		button_hbox = gtk_hbox_new (FALSE, 12);
		gtk_container_add (GTK_CONTAINER (button_hbox), real_entry);

		/* Button */
		real_button = gtk_button_new_with_mnemonic (button_text);
		gtk_container_add (GTK_CONTAINER (button_hbox), real_button);

		gtk_box_pack_start (GTK_BOX (real_hbox), button_hbox, TRUE, TRUE, 0);
	}

	if (hbox)
		*hbox = real_hbox;

	if (label)
		*label = real_label;

	if (entry)
		*entry = real_entry;

	if (button)
		*button = real_button;
}

static void
construct_widgets (GMEEntryDialog *dialog)
{
	GMEEntryDialogPrivate *priv;

	priv = GME_ENTRY_DIALOG_GET_PRIVATE (dialog);

	/* Table */
	priv->vbox = gtk_vbox_new (FALSE, 6);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox), priv->vbox);

	priv->label_size_group = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);

	/* Name */
	add_entry_hbox_to_vbox (GTK_VBOX (priv->vbox), _("_Name:"),
				priv->label_size_group,
				&priv->name_hbox,
				&priv->name_label,
				&priv->name_entry,
				NULL,
				NULL);

	g_signal_connect (priv->name_entry, "changed",
			  G_CALLBACK (entry_changed), dialog);

	/* Comment */
	add_entry_hbox_to_vbox (GTK_VBOX (priv->vbox), _("_Comment:"),
				priv->label_size_group,
				&priv->comment_hbox,
				&priv->comment_label,
				&priv->comment_entry,
				NULL,
				NULL);

	g_signal_connect (priv->comment_entry, "changed",
			  G_CALLBACK (entry_changed), dialog);

	/* Executable */
	add_entry_hbox_to_vbox (GTK_VBOX (priv->vbox), _("_Executable:"),
				priv->label_size_group,
				&priv->executable_hbox,
				&priv->executable_label,
				&priv->executable_entry,
				&priv->executable_button,
				_("_Select..."));

	/* In (Only for New Submenu Dialogs) */
	add_double_label_hbox_to_vbox (GTK_VBOX (priv->vbox), _("In:"),
				       priv->label_size_group,
				       &priv->in_hbox,
				       &priv->in_label1,
				       &priv->in_label2);

	g_signal_connect (priv->executable_entry, "changed",
			  G_CALLBACK (entry_changed), dialog);

	g_signal_connect (priv->executable_button, "clicked",
			  G_CALLBACK (entry_filechooser), dialog);

	gtk_widget_show_all (priv->vbox);
}

static void
gme_entry_dialog_get_property (GObject     *object,
			       guint        prop_id,
			       GValue      *value,
			       GParamSpec  *pspec)
{
	GMEEntryDialogPrivate *priv;

	priv = GME_ENTRY_DIALOG_GET_PRIVATE (object);

	switch (prop_id) {
		case PROP_ENTRY:
			g_value_set_pointer (value, priv->entry);
			break;

		case PROP_MODE:
			g_value_set_enum (value, priv->mode);
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}
}

static void
gme_entry_dialog_set_property (GObject      *object,
			       guint         prop_id,
			       const GValue *value,
			       GParamSpec   *pspec)
{
	switch (prop_id) {
		case PROP_ENTRY:
			update_from_entry (GME_ENTRY_DIALOG (object), g_value_get_pointer (value));
			break;

		case PROP_MODE:
			update_from_mode (GME_ENTRY_DIALOG (object), g_value_get_enum (value));
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}
}

static void
gme_entry_dialog_init (GMEEntryDialog *dialog)
{
	GMEEntryDialogPrivate *priv;

	priv = GME_ENTRY_DIALOG_GET_PRIVATE (dialog);

	construct_widgets (dialog);

	gtk_dialog_add_buttons (GTK_DIALOG (dialog),
				GTK_STOCK_HELP, GTK_RESPONSE_HELP,
				GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
				GTK_STOCK_OK, GTK_RESPONSE_OK,
				NULL);

	/* HIG Compliance */
	gtk_widget_ensure_style (GTK_WIDGET (dialog));
	gtk_container_set_border_width (GTK_CONTAINER (dialog), 12);
	gtk_box_set_spacing (GTK_BOX (GTK_DIALOG (dialog)->vbox), 12);
	gtk_box_set_spacing (GTK_BOX (GTK_DIALOG (dialog)->action_area), 6);
	gtk_container_set_border_width (GTK_CONTAINER (GTK_DIALOG (dialog)->action_area), 0);

	gtk_window_set_title (GTK_WINDOW (dialog), _("Menu Editor"));
	gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);
	gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);
	gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog), GTK_RESPONSE_OK, FALSE);

	priv->entry = NULL;
	priv->entry_name = NULL;
	priv->entry_comment = NULL;
	priv->entry_executable = NULL;
}

static void
gme_entry_dialog_finalize (GObject *object)
{
	GMEEntryDialogPrivate *priv;

	priv = GME_ENTRY_DIALOG_GET_PRIVATE (object);

	g_free (priv->entry_name);
	g_free (priv->entry_comment);
	g_free (priv->entry_executable);

	if (G_OBJECT_CLASS (gme_entry_dialog_parent_class)->finalize)
		G_OBJECT_CLASS (gme_entry_dialog_parent_class)->finalize (object);
}

static void
gme_entry_dialog_class_init (GMEEntryDialogClass *class)
{
	GObjectClass *object_class;

	object_class = G_OBJECT_CLASS (class);
	object_class->get_property = gme_entry_dialog_get_property;
	object_class->set_property = gme_entry_dialog_set_property;
	object_class->finalize = gme_entry_dialog_finalize;

	g_object_class_install_property (object_class, PROP_ENTRY,
					 g_param_spec_pointer ("entry", "Entry", NULL,
						 	       G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE));


	g_object_class_install_property (object_class, PROP_MODE,
					 g_param_spec_enum ("mode", "Mode", NULL,
						 	    GME_TYPE_ENTRY_DIALOG_MODE,
						 	    ENTRY_DIALOG_MODE_NEW_APPLICATION,
							    G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE));

	g_type_class_add_private (class, sizeof (GMEEntryDialogPrivate));
}


const char *
gme_entry_dialog_get_name (GMEEntryDialog *dialog)
{

	GMEEntryDialogPrivate *priv;

	g_return_val_if_fail (dialog, NULL);

	priv = GME_ENTRY_DIALOG_GET_PRIVATE (dialog);

	return gtk_entry_get_text (GTK_ENTRY (priv->name_entry));
}

const char *
gme_entry_dialog_get_comment (GMEEntryDialog *dialog)
{
	GMEEntryDialogPrivate *priv;

	g_return_val_if_fail (dialog, NULL);

	priv = GME_ENTRY_DIALOG_GET_PRIVATE (dialog);

	return gtk_entry_get_text (GTK_ENTRY (priv->comment_entry));

}

const char *
gme_entry_dialog_get_executable (GMEEntryDialog *dialog)
{
	GMEEntryDialogPrivate *priv;

	g_return_val_if_fail (dialog, NULL);

	priv = GME_ENTRY_DIALOG_GET_PRIVATE (dialog);

	return gtk_entry_get_text (GTK_ENTRY (priv->executable_entry));
}

void
gme_entry_dialog_set_name (GMEEntryDialog *dialog,
			   const char     *name)
{
	GMEEntryDialogPrivate *priv;

	g_return_if_fail (dialog);

	priv = GME_ENTRY_DIALOG_GET_PRIVATE (dialog);

	gtk_entry_set_text (GTK_ENTRY (priv->name_entry),
			    name ? name : "");
}

void
gme_entry_dialog_set_comment (GMEEntryDialog *dialog,
			      const char     *comment)
{
	GMEEntryDialogPrivate *priv;

	g_return_if_fail (dialog);

	priv = GME_ENTRY_DIALOG_GET_PRIVATE (dialog);

	gtk_entry_set_text (GTK_ENTRY (priv->comment_entry),
			    comment ? comment : "");
}

void
gme_entry_dialog_set_executable (GMEEntryDialog *dialog,
				 const char     *executable)
{
	GMEEntryDialogPrivate *priv;

	g_return_if_fail (dialog);

	priv = GME_ENTRY_DIALOG_GET_PRIVATE (dialog);

	gtk_entry_set_text (GTK_ENTRY (priv->executable_entry),
			    executable ? executable : "");
}

void
gme_entry_dialog_set_in (GMEEntryDialog *dialog,
			 const char     *in)
{
	GMEEntryDialogPrivate *priv;

	g_return_if_fail (dialog);

	priv = GME_ENTRY_DIALOG_GET_PRIVATE (dialog);

	gtk_label_set_text (GTK_LABEL (priv->in_label2), in);
}

GtkWidget *
gme_entry_dialog_new (GtkWindow          *parent,
		      GMenuTreeEntry     *entry,
		      GMEEntryDialogMode  mode)
{
	GtkWidget *ret;

	ret = g_object_new (GME_TYPE_ENTRY_DIALOG,
			    "entry", entry,
			    "mode", mode,
			    NULL);

	if (parent) {
		gtk_window_set_position (GTK_WINDOW (ret), GTK_WIN_POS_CENTER_ON_PARENT);
		gtk_window_set_modal (GTK_WINDOW (ret), TRUE);
		gtk_window_set_transient_for (GTK_WINDOW (ret), parent);
	}

	return ret;
}
