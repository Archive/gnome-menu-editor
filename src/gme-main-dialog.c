/*
 *  Copyright (C) 2004, 2005 Free Software Foundation, Inc.
 *
 *  Authors: Christian Neumair <chris@gnome-de.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id$
 */

#include "gme-main-dialog.h"

#include "gme-application-tree-view.h"
#include "gme-application-list-store.h"
#include "gme-entry-dialog.h"
#include "gme-menu-tree-view.h"
#include "gme-util.h"

#include <glib-object.h>
#include <glib/gi18n.h>

#include <gtk/gtk.h>

#define GMENU_I_KNOW_THIS_IS_UNSTABLE
#include <gmenu-tree.h>

G_DEFINE_TYPE (GMEMainDialog, gme_main_dialog, GTK_TYPE_DIALOG);

static void gme_main_dialog_init         (GMEMainDialog      *dialog);
static void gme_main_dialog_class_init   (GMEMainDialogClass *class);

static void gme_main_dialog_get_property (GObject      *object,
					  guint         prop_id,
					  GValue       *value,
					  GParamSpec   *pspec);
static void gme_main_dialog_set_property (GObject      *object,
					  guint         prop_id,
					  const GValue *value,
					  GParamSpec   *pspec);

enum {
	PROP_0,
	PROP_UI_MANAGER
};

extern GMenuTreeDirectory *selected_menu;
extern GMenuTreeEntry     *selected_app;

#define GME_MAIN_DIALOG_GET_PRIVATE(o) \
	(G_TYPE_INSTANCE_GET_PRIVATE ((o), GME_TYPE_MAIN_DIALOG, GMEMainDialogPrivate))

enum {
	REREAD_MENU_SIGNAL,
	LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

typedef struct _GMEMainDialogPrivate GMEMainDialogPrivate;

struct _GMEMainDialogPrivate
{
	GtkWidget *hpaned;

	/* Size Group for New/Delete Buttons */
	GtkSizeGroup *button_sizegroup;

	/* Left Menu VBox */
	GtkWidget *menu_vbox;

	/* Menu Header */
	GtkWidget *menu_label;

	/* Menu TreeView */
	GtkWidget *menu_scrolled_window;
	GtkWidget *menu_treeview;

	/* Right App VBox */
	GtkWidget *app_vbox;

	/* App Header */
	GtkWidget *app_label;

	/* App TreeView */
	GtkWidget *app_scrolled_window;
	GtkWidget *app_treeview;

	GMenuTree    *menu_tree;
	GtkUIManager *ui_manager;
};

const char *ui_data = ""
"<popup name=\"MenuPopup\">"
"  <menuitem name=\"NewMenu\"        action=\"NewMenu\"/>"
"  <menuitem name=\"NewApplication\" action=\"NewApplication\"/>"
/*"  <menuitem name=\"MenuProperties\"     action=\"MenuProperties\"/>"*/
"</popup>"
"<popup name=\"ApplicationPopup\">"
"  <menuitem name=\"NewApplication\"        action=\"NewApplication\"/>"
"  <menuitem name=\"ApplicationProperties\" action=\"ApplicationProperties\"/>"
"</popup>";

static void
gme_main_dialog_response (GtkDialog *dialog,
			  int        response)
{
	if (response != DIALOG_RESPONSE_DEFAULT)
		return;

	menu_files_delete ();
}

static void
select_row_paths (GtkTreeSelection *selection,
		  GList            *paths,
		  GtkTreeModel     *model)
{
	for (; paths; paths = paths->next) {
		GtkTreePath *path;
		GtkTreeIter  iter;

		path = (GtkTreePath *) paths->data;

		if (!gtk_tree_model_get_iter (model, &iter, path)) /* path might be invalid if it doesn't exist anymore */
			continue;

		gtk_tree_selection_select_path (selection, path);
	}

}

static void
menu_tree_changed (GMenuTree     *tree,
		   GMEMainDialog *dialog)
{
	g_signal_emit_by_name (dialog, "reread-menu");
}

static void
gme_main_dialog_reread_menu (GMEMainDialog *dialog)
{
	GMEMainDialogPrivate *priv;

	GtkTreeModel     *model;
	GtkTreeSelection *selection;
	GList            *selected_menu_row_paths;
	GList            *selected_app_row_paths;

	gboolean had_menu_tree;

	GdkCursor *cursor;

	priv = GME_MAIN_DIALOG_GET_PRIVATE (dialog);

	if (GTK_WIDGET_REALIZED (GTK_WIDGET (dialog))) { 
		GdkDisplay *display;

		display = gtk_widget_get_display (GTK_WIDGET (dialog));

		cursor = gdk_cursor_new_for_display (display, GDK_WATCH);
		gdk_window_set_cursor (GTK_WIDGET (dialog)->window, cursor);
		gdk_display_flush (display);
	}

	/* remember last selection. GtkTreeView doesn't do that for us */
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->menu_treeview));
	selected_menu_row_paths = gtk_tree_selection_get_selected_rows (selection, NULL);

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->app_treeview));
	selected_app_row_paths = gtk_tree_selection_get_selected_rows (selection, NULL);

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (priv->menu_treeview));
	gtk_tree_store_clear (GTK_TREE_STORE (model));

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (priv->app_treeview));
	model = gtk_tree_model_filter_get_model (GTK_TREE_MODEL_FILTER (model));
	gtk_list_store_clear (GTK_LIST_STORE (model));

	had_menu_tree = priv->menu_tree != NULL;
	/* actually re-read menu file */
	fill_tree_views (GTK_TREE_VIEW (priv->menu_treeview),
			 GTK_TREE_VIEW (priv->app_treeview),
			 &priv->menu_tree);
	if (!had_menu_tree && priv->menu_tree)
		gmenu_tree_add_monitor (priv->menu_tree,
					(GMenuTreeChangedFunc) menu_tree_changed,
					dialog);

	gtk_tree_view_expand_all (GTK_TREE_VIEW (priv->menu_treeview));

	/* restore last selection */
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->menu_treeview));
	select_row_paths (selection, selected_menu_row_paths, model);

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->app_treeview));
	select_row_paths (selection, selected_app_row_paths, model);

	if (GTK_WIDGET_REALIZED (GTK_WIDGET (dialog))) { 
		gdk_window_set_cursor (GTK_WIDGET (dialog)->window, NULL);

		gdk_cursor_unref (cursor);
	}

	g_list_foreach (selected_menu_row_paths,
			(GFunc) gtk_tree_path_free,
			NULL);
	g_list_free (selected_menu_row_paths);

	g_list_foreach (selected_app_row_paths,
			(GFunc) gtk_tree_path_free,
			NULL);
	g_list_free (selected_app_row_paths);


	gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog),
					   DIALOG_RESPONSE_DEFAULT,
					   menu_files_exist ());
}

static void
gme_main_dialog_finalize (GObject *object)
{
	GMEMainDialogPrivate *priv;

	priv = GME_MAIN_DIALOG_GET_PRIVATE (object);

	if (priv->button_sizegroup) {
		g_object_unref (G_OBJECT (priv->button_sizegroup));
		priv->button_sizegroup = NULL;
	}

	if (priv->menu_tree) {
		gmenu_tree_remove_monitor (priv->menu_tree,
					   (GMenuTreeChangedFunc) menu_tree_changed,
					   object);
		gmenu_tree_unref (priv->menu_tree);
		priv->menu_tree = NULL;
	}

	G_OBJECT_CLASS (gme_main_dialog_parent_class)->finalize (object);
}

static void
gme_main_dialog_class_init (GMEMainDialogClass *class)
{
	GObjectClass   *object_class;
	GtkDialogClass *dialog_class;

	object_class = G_OBJECT_CLASS (class);
	object_class->get_property = gme_main_dialog_get_property;
	object_class->set_property = gme_main_dialog_set_property;
	object_class->finalize = gme_main_dialog_finalize;

	dialog_class = GTK_DIALOG_CLASS (class);
	dialog_class->response = gme_main_dialog_response;

	class->reread_menu = gme_main_dialog_reread_menu;

	signals[REREAD_MENU_SIGNAL] = g_signal_new ("reread-menu",
						    GME_TYPE_MAIN_DIALOG,
						    G_SIGNAL_RUN_LAST,
						    G_STRUCT_OFFSET (GMEMainDialogClass, reread_menu),
						    NULL, NULL,
						    g_cclosure_marshal_VOID__VOID,
						    G_TYPE_NONE, 0);

	g_object_class_install_property (object_class, PROP_UI_MANAGER,
					 g_param_spec_object ("ui-manager", "UI Manager", NULL,
						 	      GTK_TYPE_UI_MANAGER,
							      G_PARAM_CONSTRUCT_ONLY |
							      G_PARAM_READWRITE));

	g_type_class_add_private (class, sizeof (GMEMainDialogPrivate));
}

static void
delete_app_button_toggled (GtkToggleButton *delete_app_button,
			   GMEMainDialog   *dialog)
{
	if (!gtk_toggle_button_get_active (delete_app_button))
		return;

	g_return_if_fail (selected_app);

	menu_entry_delete (selected_app);
	gtk_toggle_button_set_active (delete_app_button, FALSE);
}

static void
gme_main_dialog_get_property (GObject      *object,
			      guint         prop_id,
			      GValue       *value,
			      GParamSpec   *pspec)
{
	GMEMainDialogPrivate *priv;

	priv = GME_MAIN_DIALOG_GET_PRIVATE (object);

	switch (prop_id) {
		case PROP_UI_MANAGER:
			g_value_set_object (value, priv->ui_manager);
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
gme_main_dialog_set_property (GObject      *object,
			      guint         prop_id,
			      const GValue *value,
			      GParamSpec   *pspec)
{
	GMEMainDialogPrivate *priv;

	priv = GME_MAIN_DIALOG_GET_PRIVATE (object);

	switch (prop_id) {
		case PROP_UI_MANAGER:
			priv->ui_manager = GTK_UI_MANAGER (g_value_get_object (value));
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
construct_menu_widgets (GMEMainDialog *dialog)
{
	GMEMainDialogPrivate *priv;

	priv = GME_MAIN_DIALOG_GET_PRIVATE (dialog);

	/* container vbox */
	priv->menu_vbox = gtk_vbox_new (FALSE, 6);
	gtk_paned_add1 (GTK_PANED (priv->hpaned), priv->menu_vbox);

	/* label */
	priv->menu_label = gtk_label_new_with_mnemonic (_("_Menus:"));
	gtk_misc_set_alignment (GTK_MISC (priv->menu_label), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (priv->menu_vbox), priv->menu_label, FALSE, FALSE, 0);

	/* scrolled window */
	priv->menu_scrolled_window = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (priv->menu_scrolled_window), GTK_SHADOW_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (priv->menu_scrolled_window),
					GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (priv->menu_vbox), priv->menu_scrolled_window);

	/* tree view */
	priv->menu_treeview = gme_menu_tree_view_new (priv->ui_manager);
	gtk_label_set_mnemonic_widget (GTK_LABEL (priv->menu_label), priv->menu_treeview);
	gtk_container_add (GTK_CONTAINER (priv->menu_scrolled_window), priv->menu_treeview);
}

static void
construct_app_widgets (GMEMainDialog *dialog)
{
	GMEMainDialogPrivate *priv;
	GtkTreeSelection *selection;

	priv = GME_MAIN_DIALOG_GET_PRIVATE (dialog);

	/* container vbox */
	priv->app_vbox = gtk_vbox_new (FALSE, 6);
	gtk_paned_add2 (GTK_PANED (priv->hpaned), priv->app_vbox);

	/* label */
	priv->app_label = gtk_label_new_with_mnemonic (_("_Applications:"));
	gtk_misc_set_alignment (GTK_MISC (priv->app_label), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (priv->app_vbox), priv->app_label, FALSE, FALSE, 0);

	/* scrolled window */
	priv->app_scrolled_window = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (priv->app_scrolled_window), GTK_SHADOW_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (priv->app_scrolled_window),
					GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (priv->app_vbox), priv->app_scrolled_window);

	/* tree view */
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->menu_treeview));

	priv->app_treeview = gme_application_tree_view_new (selection, priv->ui_manager);
	gtk_label_set_mnemonic_widget (GTK_LABEL (priv->app_label), priv->app_treeview);
	gtk_container_add (GTK_CONTAINER (priv->app_scrolled_window), priv->app_treeview);
}

static void
gme_main_dialog_init (GMEMainDialog *dialog)
{
	GMEMainDialogPrivate *priv;

	priv = GME_MAIN_DIALOG_GET_PRIVATE (dialog);

	priv->menu_tree = NULL;

	/* Content Paned */
	priv->hpaned = gtk_hpaned_new ();
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox), priv->hpaned);

	priv->button_sizegroup = gtk_size_group_new (GTK_SIZE_GROUP_BOTH);

	/* Setup UI Manager */
	g_object_set (G_OBJECT (dialog), "ui-manager", gtk_ui_manager_new (), NULL);

	/* Applications */
	construct_menu_widgets (dialog);
	construct_app_widgets (dialog);

	/* Add UI definitions for popup menus. The relevant actions were
	 * added in the constructors */
	gtk_ui_manager_add_ui_from_string (priv->ui_manager, ui_data, -1, NULL);

	/* Dialog */
	gtk_dialog_add_buttons (GTK_DIALOG (dialog),
				GTK_STOCK_HELP, GTK_RESPONSE_HELP,
				_("_Default"), DIALOG_RESPONSE_DEFAULT,
				GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
				NULL);

	/* HIG Compliance */
	gtk_widget_ensure_style (GTK_WIDGET (dialog));
	gtk_container_set_border_width (GTK_CONTAINER (dialog), 12);
	gtk_box_set_spacing (GTK_BOX (GTK_DIALOG (dialog)->vbox), 12);
	gtk_box_set_spacing (GTK_BOX (GTK_DIALOG (dialog)->action_area), 6);
	gtk_container_set_border_width (GTK_CONTAINER (GTK_DIALOG (dialog)->action_area), 0);

	gtk_window_set_title (GTK_WINDOW (dialog), _("Menu Editor"));
	gtk_window_set_default_size (GTK_WINDOW (dialog), 400, 300);
	gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);

	gtk_container_foreach (GTK_CONTAINER (dialog),
			       (GtkCallback) gtk_widget_show_all,
			       NULL);
}

GtkWidget *
gme_main_dialog_new (void)
{
	GtkWidget *ret;

	ret = g_object_new (GME_TYPE_MAIN_DIALOG, NULL);
	g_signal_emit_by_name (ret, "reread-menu");

	return ret;
}
