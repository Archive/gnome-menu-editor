/*
 *  Copyright (C) 2004, 2005 Free Software Foundation, Inc.
 *
 *  Authors: Christian Neumair <chris@gnome-de.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id$
 */

#include "gme-application-tree-model-filter.h"

#include "gme-application-list-store.h"
#include "gme-menu-tree-store.h"

#include <glib-object.h>

#include <gtk/gtk.h>

#define GMENU_I_KNOW_THIS_IS_UNSTABLE
#include <gmenu-tree.h>

G_DEFINE_TYPE (GMEApplicationTreeModelFilter,
	       gme_application_tree_model_filter,
	       GTK_TYPE_TREE_MODEL_FILTER);

static void gme_application_tree_model_filter_init       (GMEApplicationTreeModelFilter      *model);
static void gme_application_tree_model_filter_class_init (GMEApplicationTreeModelFilterClass *class);

static void gme_application_tree_model_filter_get_property (GObject      *object,
							    guint         prop_id,
							    GValue       *value,
							    GParamSpec   *pspec);
static void gme_application_tree_model_filter_set_property (GObject      *object,
							    guint         prop_id,
							    const GValue *value,
							    GParamSpec   *pspec);

enum {
	PROP_0,
	PROP_FILTER_SELECTION
};

GMenuTreeDirectory *selected_menu;

static gboolean
selection_refilter_foreach (GtkTreeModel           *model,
			    GtkTreeIter            *iter,
			    G_GNUC_UNUSED gpointer  data)
{
	gpointer entry;
	gboolean display;

	gtk_tree_model_get (model, iter,
			    GME_APPLICATION_LIST_STORE_COL_MENU_TREE_ENTRY, &entry, 
			    -1);
	if (!entry)
		return TRUE;

	return (gmenu_tree_item_get_parent (GMENU_TREE_ITEM (entry)) == selected_menu);
}

static void
filter_selection_changed (GtkTreeSelection              *selection,
			  GMEApplicationTreeModelFilter *model)
{
	/* redo filtering */
	gtk_tree_model_filter_refilter (GTK_TREE_MODEL_FILTER (model));
}

static void
gme_application_tree_model_filter_get_property (GObject     *object,
						guint        prop_id,
						GValue      *value,
						GParamSpec  *pspec)
{
	GMEApplicationTreeModelFilter *model = GME_APPLICATION_TREE_MODEL_FILTER (object);

	switch (prop_id) {
		case PROP_FILTER_SELECTION:
			g_value_set_object (value, model->filter_selection);
			break;

		default:
			g_assert_not_reached ();
			break;
	}

}

static void
gme_application_tree_model_filter_set_property (GObject      *object,
						guint         prop_id,
						const GValue *value,
						GParamSpec   *pspec)
{
	GMEApplicationTreeModelFilter *model = GME_APPLICATION_TREE_MODEL_FILTER (object);

	switch (prop_id) {
		case PROP_FILTER_SELECTION:
			if (model->filter_selection) {
				g_signal_handlers_disconnect_by_func (model->filter_selection,
								      filter_selection_changed,
								      model);
			}

			model->filter_selection = GTK_TREE_SELECTION (g_value_get_object (value));
			g_assert (model->filter_selection);
			g_signal_connect_after (model->filter_selection, "changed",
						G_CALLBACK (filter_selection_changed), model);
			break;

		default:
			g_assert_not_reached ();
			break;
	}
}

static void
gme_application_tree_model_filter_init (GMEApplicationTreeModelFilter *model)
{
	gtk_tree_model_filter_set_visible_func (GTK_TREE_MODEL_FILTER (model),
						selection_refilter_foreach,
						NULL,
						NULL);
}

static void
gme_application_tree_model_filter_class_init (GMEApplicationTreeModelFilterClass *class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);

	object_class->get_property = gme_application_tree_model_filter_get_property;
	object_class->set_property = gme_application_tree_model_filter_set_property;

	g_object_class_install_property (object_class, PROP_FILTER_SELECTION,
					 g_param_spec_object ("filter-selection",
						 	      "Filter Selection",
							      "This selection (from the menu tree view) will be used to determine "
							      "whether an entry will be shown",
							      GTK_TYPE_TREE_SELECTION,
							      G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));
}

GtkTreeModel *
gme_application_tree_model_filter_new (GtkTreeSelection *filter_selection)
{
	GtkTreeModel *child_model = gme_application_list_store_new ();

	return g_object_new (GME_TYPE_APPLICATION_TREE_MODEL_FILTER,
			     "child-model", child_model,
			     "virtual-root", NULL,
			     "filter-selection", filter_selection,
			     NULL);
}
