/*
 *  Copyright (C) 2004, 2005 Free Software Foundation, Inc.
 *
 *  Authors: Christian Neumair <chris@gnome-de.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id$
 */

#include "gme-menu-tree-store.h"

#include <glib-object.h>

#include <gdk/gdkpixbuf.h>

#include <gtk/gtk.h>

static void gme_menu_tree_store_init (GMEMenuTreeStore *store);
static void gme_menu_tree_store_init (GMEMenuTreeStore *store);

G_DEFINE_TYPE (GMEMenuTreeStore,
	       gme_menu_tree_store,
	       GTK_TYPE_TREE_STORE);

static void
gme_menu_tree_store_init (GMEMenuTreeStore *store)
{
	GType column_types[] = {
		G_TYPE_POINTER,  /* GME_MENU_TREE_STORE_COL_MENU */
		G_TYPE_STRING,   /* GME_MENU_TREE_STORE_COL_MENU_NAME */
		GDK_TYPE_PIXBUF, /* GME_MENU_TREE_STORE_COL_MENU_PIXBUF */
		G_TYPE_BOOLEAN   /* GME_MENU_TREE_STORE_COL_MENU_IS_EMPTY */
	};

	gtk_tree_store_set_column_types (GTK_TREE_STORE (store),
					 GME_MENU_TREE_STORE_N_COLUMNS,
					 column_types);

	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (store),
					      GME_MENU_TREE_STORE_COL_MENU_NAME,
					      GTK_SORT_ASCENDING);
}

static void
gme_menu_tree_store_class_init (GMEMenuTreeStoreClass *class)
{
}

GtkTreeStore *
gme_menu_tree_store_new (void)
{
	return g_object_new (GME_TYPE_MENU_TREE_STORE, NULL);
}
