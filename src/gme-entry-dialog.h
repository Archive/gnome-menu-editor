/*
 *  Copyright (C) 2004, 2005 Free Software Foundation, Inc.
 *
 *  Authors: Christian Neumair <chris@gnome-de.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id$
 */

#ifndef GME_ENTRY_DIALOG_H
#define GME_ENTRY_DIALOG_H

#include <gtk/gtkdialog.h>
#include <gtk/gtkwidget.h>

#define GMENU_I_KNOW_THIS_IS_UNSTABLE
#include <gmenu-tree.h>

#define GME_TYPE_ENTRY_DIALOG            (gme_entry_dialog_get_type ())
#define GME_ENTRY_DIALOG(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_ENTRY_DIALOG, GMEEntryDialog))
#define GME_ENTRY_DIALOG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_ENTRY_DIALOG, GMEEntryDialog))
#define GME_IS_ENTRY_DIALOG(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_ENTRY_DIALOG))
#define GME_IS_ENTRY_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_ENTRY_DIALOG))
#define GME_ENTRY_DIALOG_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_ENTRY_DIALOG, GMEEntryDialog))

#define GME_TYPE_ENTRY_DIALOG_MODE       (gme_entry_dialog_mode_get_type ())

typedef struct _GMEEntryDialog      GMEEntryDialog;
typedef struct _GMEEntryDialogClass GMEEntryDialogClass;

typedef enum {
	ENTRY_DIALOG_MODE_NEW_APPLICATION,
	ENTRY_DIALOG_MODE_NEW_SUBMENU,
	ENTRY_DIALOG_MODE_EDIT_APPLICATION,
	ENTRY_DIALOG_MODE_EDIT_MENU
} GMEEntryDialogMode;

struct _GMEEntryDialog
{
	GtkDialog parent;
};

struct _GMEEntryDialogClass
{
	GtkDialogClass parent_class;
};

GtkWidget *gme_entry_dialog_new (GtkWindow           *parent,
				 GMenuTreeEntry      *entry,
				 GMEEntryDialogMode  mode);

const gchar *gme_entry_dialog_get_name       (GMEEntryDialog *dialog);
const gchar *gme_entry_dialog_get_comment    (GMEEntryDialog *dialog);
const gchar *gme_entry_dialog_get_executable (GMEEntryDialog *dialog);

void         gme_entry_dialog_set_name       (GMEEntryDialog *dialog,
					      const char     *name);
void         gme_entry_dialog_set_comment    (GMEEntryDialog *dialog,
					      const char     *comment);
void         gme_entry_dialog_set_executable (GMEEntryDialog *dialog,
					      const char     *executable);
void         gme_entry_dialog_get_in         (GMEEntryDialog *dialog,
					      const char     *in);

#endif
