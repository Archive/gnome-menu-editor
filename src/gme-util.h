/*
 *  Copyright (C) 2004, 2005 Free Software Foundation, Inc.
 *
 *  Authors: Christian Neumair <chris@gnome-de.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id$
 */

#ifndef GME_UTIL_H
#define GME_UTIL_H

#include <glib/gtypes.h>
#include <gtk/gtktreeview.h>

#define GMENU_I_KNOW_THIS_IS_UNSTABLE
#include <gmenu-tree.h>

void gme_util_init    (void);
void gme_util_cleanup (void);

GtkTargetEntry *matching_context_target (GdkDragContext *context,
					 GtkTargetEntry *targets,
					 unsigned int    n_targets);

gboolean fill_tree_views (GtkTreeView   *directory_tree_view,
			  GtkTreeView   *application_tree_view,
			  GMenuTree    **tree);

gboolean menu_file_add_entry (const char         *name,
			      const char         *exec,
			      const char         *comment,
			      GMenuTreeDirectory *parent_dir);
gboolean menu_file_add_menu  (const char         *name,
			      GMenuTreeDirectory *parent_dir);
gboolean menu_file_set_display_entry (GMenuTreeEntry  *entry,
				      gboolean         display);
gboolean menu_file_copy_entry (GMenuTreeEntry     *entry,
			       GMenuTreeDirectory *target);
void     menu_files_delete (void);
gboolean menu_files_exist  (void);

void     menu_entry_change         (GMenuTreeEntry *entry,
				    const char     *name,
				    const char     *exec,
				    const char     *comment);
gboolean menu_entry_can_be_deleted (GMenuTreeEntry *entry);
void     menu_entry_delete         (GMenuTreeEntry *entry);

#endif /* GME_UTIL_H */
