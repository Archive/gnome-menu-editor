/*
 *  Copyright (C) 2004, 2005 Free Software Foundation, Inc.
 *
 *  Authors: Christian Neumair <chris@gnome-de.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id$
 */

#include "gme-main-dialog.h"

#include <config.h>
#ifdef HAVE_LOCALE_H
#include <locale.h>
#endif /* HAVE_LOCALE_H */

#include <glib/ghash.h>
#include <glib/gi18n.h>

#include <gtk/gtk.h>

#include <libgnomevfs/gnome-vfs-init.h>

extern GHashTable *icon_pixbufs;

int main (int argc, char *argv[])
{
	GtkWidget    *main_dialog;
	unsigned int  response;

#ifdef HAVE_LOCALE_H
	setlocale (LC_ALL, NULL);
#endif /* HAVE_LOCALE_H */
	bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
	textdomain (GETTEXT_PACKAGE);

	gtk_init (&argc, &argv);
	gnome_vfs_init ();
	gme_util_init ();

	main_dialog = gme_main_dialog_new ();

	response = DIALOG_RESPONSE_DEFAULT;
	while (response == DIALOG_RESPONSE_DEFAULT ||
	       response == GTK_RESPONSE_HELP)
		response = gtk_dialog_run (GTK_DIALOG (main_dialog));

	gtk_widget_destroy (GTK_WIDGET (main_dialog));

	gme_util_cleanup ();
}
