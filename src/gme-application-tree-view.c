/*
 *  Copyright (C) 2004, 2005 Free Software Foundation, Inc.
 *
 *  Authors: Christian Neumair <chris@gnome-de.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id$
 */

#include "gme-application-tree-view.h"

#include "gme-application-list-store.h"
#include "gme-application-tree-model-filter.h"
#include "gme-entry-dialog.h"
#include "gme-util.h"

#include <glib-object.h>
#include <glib/gi18n.h>

#include <gtk/gtk.h>

#include <libgnomevfs/gnome-vfs-ops.h>
#include <libgnomevfs/gnome-vfs-uri.h>

#define GMENU_I_KNOW_THIS_IS_UNSTABLE
#include <gmenu-tree.h>

G_DEFINE_TYPE (GMEApplicationTreeView,
	       gme_application_tree_view,
	       GTK_TYPE_TREE_VIEW);

/* Constructors */
static void gme_application_tree_view_init       (GMEApplicationTreeView      *view);
static void gme_application_tree_view_class_init (GMEApplicationTreeViewClass *class);

/* Inherited from GObject */
static void gme_application_tree_view_get_property (GObject      *object,
						    guint         prop_id,
						    GValue       *value,
						    GParamSpec   *pspec);
static void gme_application_tree_view_set_property (GObject      *object,
						    guint         prop_id,
						    const GValue *value,
						    GParamSpec   *pspec);

/* Inherited from GtkWidget */
static gboolean gme_application_tree_view_button_press       (GtkWidget      *widget,
							      GdkEventButton *event);
static gboolean gme_application_tree_view_popup_menu         (GtkWidget      *widget);

static gboolean gme_application_tree_view_drag_drop          (GtkWidget      *widget,
							      GdkDragContext *context,
							      gint            x,
							      gint            y,
							      guint           time);
static gboolean gme_application_tree_view_drag_motion        (GtkWidget      *widget,
						       	      GdkDragContext *context,
							      int             x,
							      int             y,
							      guint           time);
static void     gme_application_tree_view_drag_data_received (GtkWidget        *widget,
							      GdkDragContext   *context,
							      gint              x,
							      gint              y,
							      GtkSelectionData *selection,
							      guint             info,
							      guint             time);

/* Action callbacks we expose to the dialog UI Manager */
static void new_application_cb        (GtkAction *action, GMEApplicationTreeView *treeview);
static void application_properties_cb (GtkAction *action, GMEApplicationTreeView *treeview);

enum {
	PROP_0,
	PROP_UI_MANAGER
};

#define GME_APPLICATION_TREE_VIEW_GET_PRIVATE(o) \
	(G_TYPE_INSTANCE_GET_PRIVATE ((o), GME_TYPE_APPLICATION_TREE_VIEW, GMEApplicationTreeViewPrivate))

typedef struct _GMEApplicationTreeViewPrivate GMEApplicationTreeViewPrivate;

struct _GMEApplicationTreeViewPrivate {
	/* display column */
	GtkTreeViewColumn *display_column;
	GtkCellRenderer   *display_renderer;

	/* name column */
	GtkTreeViewColumn *name_column;
	GtkCellRenderer *pixbuf_renderer;
	GtkCellRenderer *text_renderer;

	GtkActionGroup *action_group;
	GtkUIManager   *ui_manager;
};

static GtkTargetEntry drag_targets[] = {
	"x-internal/application", 0, 0
};

static GtkTargetEntry drop_targets[] = {
	"text/uri-list", 0, 0
};

GtkActionEntry action_entries[] = {
	{ "NewApplication",        GTK_STOCK_NEW,        N_("_New Application..."), NULL, NULL, G_CALLBACK (new_application_cb) },
	{ "ApplicationProperties", GTK_STOCK_PROPERTIES, NULL,                      NULL, NULL, G_CALLBACK (application_properties_cb) }
};

GMenuTreeDirectory *selected_menu;
GMenuTreeEntry     *selected_app = NULL;

static void
display_renderer_toggled (GtkCellRendererToggle  *cell_renderer,
			  char                   *path_string,
			  GMEApplicationTreeView *view)
{
	GtkTreeModel *model;
	GtkListStore *list_store;
	GtkTreeIter   iter;
	GtkTreePath  *path;

	gboolean active;

	GMenuTreeEntry *entry;

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (view));
	if (!gtk_tree_model_get_iter_from_string (model, &iter, path_string))
		return;

	/* fetch menu tree entry */
	gtk_tree_model_get (model, &iter,
			    GME_APPLICATION_LIST_STORE_COL_MENU_TREE_ENTRY, &entry,
			    -1);

	active = !gtk_cell_renderer_toggle_get_active (cell_renderer);

	/* update the menu file */
	menu_file_set_display_entry (entry, active);
}

static void
selection_changed (GtkTreeSelection *selection)
{
	GtkTreeModel *model;
	GtkTreeIter   iter;

	if (gtk_tree_selection_get_selected (selection, &model, &iter))
		gtk_tree_model_get (model, &iter,
				    GME_APPLICATION_LIST_STORE_COL_MENU_TREE_ENTRY, &selected_app,
				    -1);
	else
		selected_app = NULL;
}

static void
new_application_cb (GtkAction              *action,
		    GMEApplicationTreeView *view)
{
	GtkWidget *toplevel;
	GtkWidget *dialog;
	int        response;

	toplevel = gtk_widget_get_toplevel (GTK_WIDGET (view));

	dialog = gme_entry_dialog_new (GTK_WINDOW (toplevel), NULL, ENTRY_DIALOG_MODE_NEW_APPLICATION);
	while ((response = gtk_dialog_run (GTK_DIALOG (dialog))) == GTK_RESPONSE_HELP)
		;
	if (response == GTK_RESPONSE_OK)
		menu_file_add_entry (gme_entry_dialog_get_name (GME_ENTRY_DIALOG (dialog)),
				     gme_entry_dialog_get_executable (GME_ENTRY_DIALOG (dialog)),
				     gme_entry_dialog_get_comment (GME_ENTRY_DIALOG (dialog)),
				     selected_menu);

	gtk_widget_destroy (dialog);
}

static void
application_properties_cb (GtkAction              *action,
			   GMEApplicationTreeView *view)
{
	GtkWidget *toplevel;
	GtkWidget *dialog;
	gint       response;

	toplevel = gtk_widget_get_toplevel (GTK_WIDGET (view));

	dialog = gme_entry_dialog_new (GTK_WINDOW (toplevel), selected_app, ENTRY_DIALOG_MODE_EDIT_APPLICATION);
	while ((response = gtk_dialog_run (GTK_DIALOG (dialog))) == GTK_RESPONSE_HELP)
		;
	if (response == GTK_RESPONSE_OK)
		menu_entry_change (selected_app,
				   gme_entry_dialog_get_name (GME_ENTRY_DIALOG (dialog)),
				   gme_entry_dialog_get_executable (GME_ENTRY_DIALOG (dialog)),
				   gme_entry_dialog_get_comment (GME_ENTRY_DIALOG (dialog)));

	gtk_widget_destroy (dialog);
}

static void
update_action_sensitivity (GMEApplicationTreeView *view)
{
	GMEApplicationTreeViewPrivate *priv;

	GtkAction *action;

	priv = GME_APPLICATION_TREE_VIEW_GET_PRIVATE (view);

	action = gtk_action_group_get_action (priv->action_group, "ApplicationProperties");
	gtk_action_set_sensitive (action, selected_app != NULL);
}

static void
show_application_popup (GMEApplicationTreeView *view,
			GdkEventButton         *event)
{
	GMEApplicationTreeViewPrivate *priv;

	GtkWidget *menu;
	int        event_button;
	int        event_time;

	priv = GME_APPLICATION_TREE_VIEW_GET_PRIVATE (view);

	if (event) {
		event_button = event->button;
		event_time = event->time;
	}
	else {
		event_button = 0;
		event_time = gtk_get_current_event_time();
	}

	update_action_sensitivity (view);

	menu = gtk_ui_manager_get_widget (priv->ui_manager, "/ApplicationPopup");
	gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL,
			event_button, event_time);
}

static gboolean
gme_application_tree_view_button_press (GtkWidget      *widget,
					GdkEventButton *event)
{
	GtkTreeSelection *selection;
	gboolean          ret;

	ret = GTK_WIDGET_CLASS (gme_application_tree_view_parent_class)->button_press_event (widget, event);

	if (event->button == 3) {
		show_application_popup (GME_APPLICATION_TREE_VIEW (widget), event);

		ret = TRUE;
	}

	return ret;
}

static gboolean
gme_application_tree_view_popup_menu (GtkWidget *widget)
{
	show_application_popup (GME_APPLICATION_TREE_VIEW (widget), NULL);

	return TRUE;
}

static void
gme_application_tree_view_drag_data_get (GtkWidget        *widget,
					 GdkDragContext   *context,
					 GtkSelectionData *data,
					 guint             info,
					 guint             time)
{
	GtkTreeSelection *selection;
	GtkTreeModel     *model;
	GtkTreeIter       iter;

	GMenuTreeEntry *entry;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (widget));

	gtk_tree_selection_get_selected (selection, &model, &iter);
	g_return_if_fail (model);
	g_return_if_fail (&iter);

	gtk_tree_model_get (model, &iter,
			    GME_APPLICATION_LIST_STORE_COL_MENU_TREE_ENTRY, &entry,
			    -1);
	g_return_if_fail (entry);

	gtk_selection_data_set (data, gdk_atom_intern ("x-internal/application", FALSE),
				8, (void *) &entry, sizeof (entry));
}

/* only allow drop of local executables and shell scripts, if one URI is passed */
static gboolean
dropped_uris_are_valid (char     **uris,
			gboolean  *is_desktop_file)
{
	GnomeVFSURI *uri;
	gboolean     ret;
	gboolean     real_is_desktop_file;

	if (!uris || !uris[0] || (uris[1] && uris[1][0] != '\0')) {
		if (is_desktop_file)
			*is_desktop_file = FALSE;

		return FALSE;
	}

	uri = gnome_vfs_uri_new (uris[0]);

	if (gnome_vfs_uri_is_local (uri)) {
		GnomeVFSFileInfo info;

		gnome_vfs_get_file_info_uri (uri, &info,
					     GNOME_VFS_FILE_INFO_GET_MIME_TYPE |
					     GNOME_VFS_FILE_INFO_FORCE_SLOW_MIME_TYPE |
					     GNOME_VFS_FILE_INFO_FOLLOW_LINKS);

		if (!info.mime_type) {
			real_is_desktop_file = FALSE;
			ret = FALSE;
		}
		else if (!strcmp (info.mime_type, "application/x-desktop")) {
			real_is_desktop_file = TRUE;
			ret = TRUE;
		}
		else if (!strcmp (info.mime_type, "application/x-executable") ||
			 !strcmp (info.mime_type, "application/x-shellscript")) {
			real_is_desktop_file = FALSE;
			ret = TRUE;
		}
		else {
			real_is_desktop_file = FALSE;
			ret = FALSE;
		}
	}
	else {
		real_is_desktop_file = FALSE;
		ret = FALSE;
	}

	if (is_desktop_file)
		*is_desktop_file = real_is_desktop_file;

	gnome_vfs_uri_unref (uri);

	return ret;
}

static void
gme_application_tree_view_drag_data_received (GtkWidget        *widget,
					      GdkDragContext   *context,
					      gint              x,
					      gint              y,
					      GtkSelectionData *selection,
					      guint             info,
					      guint             time)
{
	GtkTargetEntry *target;

	g_return_if_fail (selection);
	g_return_if_fail (selection->length >= 0);

	target = matching_context_target (context, drop_targets, G_N_ELEMENTS (drop_targets));
	g_return_if_fail (target);

	/* we got an URI list during motion and check whether it may be dropped */
	if (!strcmp (target->target, "text/uri-list")) {
		char     **uris;
		gboolean   is_desktop_file;

		uris = g_strsplit (selection->data, "\r\n", 0);

		if (!dropped_uris_are_valid (uris, &is_desktop_file)) {
			g_object_set_data (G_OBJECT (context),
					   "drop-uri-retrieval-failed",
					   GUINT_TO_POINTER (TRUE));
			context->action = 0;
		}
		else {
			g_object_set_data_full (G_OBJECT (context), "drop-uri",
						g_strdup (uris[0]), g_free);
			g_object_set_data (G_OBJECT (context), "drop-uri-is-desktop-file",
					   GUINT_TO_POINTER (is_desktop_file));
			context->action = GDK_ACTION_LINK;

			gtk_drag_highlight (GTK_WIDGET (widget->parent));
		}

		gdk_drag_status (context, context->action, time);

		g_strfreev (uris);
	}
}

static gboolean
gme_application_tree_view_drag_drop (GtkWidget      *widget,
                                     GdkDragContext *context,
                                     gint            x,
				     gint            y,
                                     guint           time)
{
	GtkWidget *toplevel;
	GtkWidget *dialog;
	gboolean   is_desktop_file;
	gint       response;

	char *uri, *name, *comment, *exec;

	g_return_val_if_fail (context->action, FALSE);

	uri = g_object_steal_data (G_OBJECT (context), "drop-uri");
	g_return_val_if_fail (uri, FALSE);

	toplevel = gtk_widget_get_toplevel (widget);

	is_desktop_file = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (context), "drop-uri-is-desktop-file"));

	dialog = gme_entry_dialog_new (GTK_WINDOW (toplevel), NULL, ENTRY_DIALOG_MODE_NEW_APPLICATION);

	if (is_desktop_file) {
		char    *filename;
		GKeyFile *keyfile;

		filename = g_filename_from_uri (uri, NULL, NULL);

		keyfile = g_key_file_new ();
		g_key_file_load_from_file (keyfile, filename,
					   G_KEY_FILE_NONE, NULL);

		name = g_key_file_get_locale_string (keyfile, "Desktop Entry", "Name", NULL, NULL);
		comment = g_key_file_get_locale_string (keyfile, "Desktop Entry", "Comment", NULL, NULL);
		exec = g_key_file_get_locale_string (keyfile, "Desktop Entry", "Exec", NULL, NULL);

		g_key_file_free (keyfile);
		g_free (filename);
	}
	else {
		comment = NULL;
		exec = g_filename_from_uri (uri, NULL, NULL);
		name = g_path_get_basename (exec);
	}

	gme_entry_dialog_set_name (GME_ENTRY_DIALOG (dialog), name);
	gme_entry_dialog_set_comment (GME_ENTRY_DIALOG (dialog), comment);
	gme_entry_dialog_set_executable (GME_ENTRY_DIALOG (dialog), exec);

	while ((response = gtk_dialog_run (GTK_DIALOG (dialog))) == GTK_RESPONSE_HELP)
		;

	if (response == GTK_RESPONSE_OK)
		menu_file_add_entry (gme_entry_dialog_get_name (GME_ENTRY_DIALOG (dialog)),
				     gme_entry_dialog_get_comment (GME_ENTRY_DIALOG (dialog)),
				     gme_entry_dialog_get_executable (GME_ENTRY_DIALOG (dialog)),
				     selected_menu);

	gtk_widget_destroy (dialog);

	gtk_drag_finish (context, (response == GTK_RESPONSE_OK), FALSE, time);

	g_free (uri);
	g_free (name);
	g_free (exec);

	return TRUE;
}

static gboolean
gme_application_tree_view_drag_motion (GtkWidget      *widget,
				       GdkDragContext *context,
				       int             x,
				       int             y,
				       guint           time)
{
	GtkTargetEntry *target;
	GtkTreePath    *path;

	if (!selected_menu)
		return FALSE;

	target = matching_context_target (context, drop_targets, G_N_ELEMENTS (drop_targets));
	if (!target) {
		gdk_drag_status (context, 0, time);

		return TRUE;
	}


	if (!strcmp (target->target, "text/uri-list")) {
		/* request URI if it hasn't already been tried */
		if (!g_object_get_data (G_OBJECT (context), "drop-uri") &&
		    !g_object_get_data (G_OBJECT (context), "drop-uri-retrieval-failed")) {
			gtk_drag_get_data (widget, context, gdk_atom_intern (target->target, FALSE), time);
		}

		return TRUE;
	}

	return FALSE;
}

static void
gme_application_tree_view_drag_leave (GtkWidget      *widget,
				      GdkDragContext *context,
				      guint           time)
{
	g_object_set_data (G_OBJECT (context), "drop-uri-retrieval-failed", NULL);

	gtk_drag_unhighlight (GTK_WIDGET (widget->parent));
}

/* Display entry properties on row activation */
static void
gme_application_tree_view_row_activated (GtkTreeView       *view,
					 GtkTreePath       *path,
					 GtkTreeViewColumn *column)
{
        GMEApplicationTreeViewPrivate *priv;
	GtkAction *action;

	priv = GME_APPLICATION_TREE_VIEW_GET_PRIVATE (view);

	action = gtk_action_group_get_action (priv->action_group, "ApplicationProperties");
	gtk_action_activate (action);
}

static void
gme_application_tree_view_get_property (GObject      *object,
					guint         prop_id,
					GValue       *value,
					GParamSpec   *pspec)
{
	GMEApplicationTreeViewPrivate *priv;

	priv = GME_APPLICATION_TREE_VIEW_GET_PRIVATE (object);

	switch (prop_id) {
		case PROP_UI_MANAGER:
			g_value_set_object (value, priv->ui_manager);
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
setup_action_group (GMEApplicationTreeView *view)
{
	GMEApplicationTreeViewPrivate *priv;

	priv = GME_APPLICATION_TREE_VIEW_GET_PRIVATE (view);

	priv->action_group = gtk_action_group_new ("ApplicationGroup");
	gtk_action_group_set_translation_domain (priv->action_group, NULL);
	gtk_action_group_add_actions (priv->action_group, action_entries,
				      G_N_ELEMENTS (action_entries), view);
}

static void
gme_application_tree_view_set_property (GObject      *object,
					guint         prop_id,
					const GValue *value,
					GParamSpec   *pspec)
{
	GMEApplicationTreeViewPrivate *priv;

	priv = GME_APPLICATION_TREE_VIEW_GET_PRIVATE (object);

	switch (prop_id) {
		case PROP_UI_MANAGER:
			if (GTK_IS_UI_MANAGER (priv->ui_manager) &&
			    GTK_IS_ACTION_GROUP (priv->action_group))
				gtk_ui_manager_remove_action_group (priv->ui_manager, priv->action_group);

			priv->ui_manager = GTK_UI_MANAGER (g_value_get_object (value));
			setup_action_group (GME_APPLICATION_TREE_VIEW (object));
			gtk_ui_manager_insert_action_group (priv->ui_manager, priv->action_group, 0);
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
gme_application_tree_view_init (GMEApplicationTreeView *view)
{
	GMEApplicationTreeViewPrivate *priv;

	GtkTreeSelection *selection;

	priv = GME_APPLICATION_TREE_VIEW_GET_PRIVATE (view);

	/* display column */
	priv->display_column = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (priv->display_column, _("Display"));
	gtk_tree_view_append_column (GTK_TREE_VIEW (view), priv->display_column);

	/* display column, toggle cell renderer */
	priv->display_renderer = gtk_cell_renderer_toggle_new ();
	gtk_tree_view_column_pack_start (priv->display_column, priv->display_renderer, TRUE);
	gtk_tree_view_column_add_attribute (priv->display_column, priv->display_renderer,
					    "active", GME_APPLICATION_LIST_STORE_COL_DISPLAY);
	g_signal_connect  (priv->display_renderer, "toggled",
			   G_CALLBACK (display_renderer_toggled), view);


	/* name column */
	priv->name_column = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (priv->name_column, _("Name"));
	gtk_tree_view_column_set_spacing (priv->name_column, 6);
	gtk_tree_view_append_column (GTK_TREE_VIEW (view), priv->name_column);

	/* name column, pixbuf cell renderer */
	priv->pixbuf_renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (priv->name_column, priv->pixbuf_renderer, FALSE);
	gtk_tree_view_column_add_attribute (priv->name_column, priv->pixbuf_renderer,
					    "pixbuf", GME_APPLICATION_LIST_STORE_COL_PIXBUF);

	/* name column, text cell renderer */
	priv->text_renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_end (priv->name_column, priv->text_renderer, TRUE);
	gtk_tree_view_column_add_attribute (priv->name_column, priv->text_renderer,
					    "text", GME_APPLICATION_LIST_STORE_COL_APPLICATION_NAME);

	/* drag and drop */
	gtk_tree_view_enable_model_drag_source (GTK_TREE_VIEW (view), GDK_BUTTON1_MASK,
						drag_targets, G_N_ELEMENTS (drag_targets),
						GDK_ACTION_COPY);
	gtk_tree_view_enable_model_drag_dest (GTK_TREE_VIEW (view),
					      drop_targets, G_N_ELEMENTS (drop_targets),
					      GDK_ACTION_LINK);

	/* update selected_app variable */
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (view));
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_BROWSE);
	g_signal_connect (selection, "changed",
			  G_CALLBACK (selection_changed),
			  NULL);
}

static void
gme_application_tree_view_class_init (GMEApplicationTreeViewClass *class)
{
	GObjectClass     *object_class;
	GtkWidgetClass   *widget_class;
	GtkTreeViewClass *tree_view_class;

	object_class = G_OBJECT_CLASS (class);
	object_class->get_property = gme_application_tree_view_get_property;
	object_class->set_property = gme_application_tree_view_set_property;

	widget_class = GTK_WIDGET_CLASS (class);
	widget_class->button_press_event = gme_application_tree_view_button_press;

	/* drag */
	widget_class->drag_data_get = gme_application_tree_view_drag_data_get;

	/* drop */
	widget_class->drag_data_received = gme_application_tree_view_drag_data_received;
	widget_class->drag_drop = gme_application_tree_view_drag_drop;
	widget_class->drag_motion = gme_application_tree_view_drag_motion;
	widget_class->drag_leave = gme_application_tree_view_drag_leave;

	tree_view_class = GTK_TREE_VIEW_CLASS (class);
	tree_view_class->row_activated = gme_application_tree_view_row_activated;

	g_object_class_install_property (object_class, PROP_UI_MANAGER,
					 g_param_spec_object ("ui-manager", "UI Manager", NULL,
						 	      GTK_TYPE_UI_MANAGER,
							      G_PARAM_CONSTRUCT_ONLY |
							      G_PARAM_READWRITE));

	g_type_class_add_private (class, sizeof (GMEApplicationTreeViewPrivate));
}

GtkWidget *
gme_application_tree_view_new (GtkTreeSelection *filter_selection,
			       GtkUIManager     *ui_manager)
{
	return g_object_new (GME_TYPE_APPLICATION_TREE_VIEW,
			     "model", gme_application_tree_model_filter_new (filter_selection),
			     "ui-manager", ui_manager,
			     NULL);
}
