/*
 *  Copyright (C) 2004, 2005 Free Software Foundation, Inc.
 *
 *  Authors: Christian Neumair <chris@gnome-de.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id$
 */

#ifndef GME_APPLICATION_TREE_MODEL_FILTER_H
#define GME_APPLICATION_TREE_MODEL_FILTER_H

#include <gtk/gtktreemodelfilter.h>
#include <gtk/gtktreeselection.h>

#define GME_TYPE_APPLICATION_TREE_MODEL_FILTER            (gme_application_tree_model_filter_get_type ())
#define GME_APPLICATION_TREE_MODEL_FILTER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_APPLICATION_TREE_MODEL_FILTER, GMEApplicationTreeModelFilter))
#define GME_APPLICATION_TREE_MODEL_FILTER_CLASS(class)    (G_TYPE_CHECK_CLASS_CAST ((class),  GME_TYPE_APPLICATION_TREE_MODEL_FILTER, GMEApplicationTreeModelFilterClass))
#define GME_IS_APPLICATION_TREE_MODEL_FILTER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_APPLICATION_TREE_MODEL_FILTER))
#define GME_IS_APPLICATION_TREE_MODEL_FILTER_CLASS(class) (G_TYPE_CHECK_CLASS_TYPE ((class),  GME_TYPE_APPLICATION_TREE_MODEL_FILTER))
#define GME_APPLICATION_TREE_MODEL_FILTER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GME_TYPE_APPLICATION_TREE_MODEL_FILTER, GMEApplicationTreeModelFilterClass))

typedef struct _GMEApplicationTreeModelFilter      GMEApplicationTreeModelFilter;
typedef struct _GMEApplicationTreeModelFilterClass GMEApplicationTreeModelFilterClass;

struct _GMEApplicationTreeModelFilter {
	GtkTreeModelFilter parent;

	GtkTreeSelection *filter_selection;
};

struct _GMEApplicationTreeModelFilterClass {
	GtkTreeModelFilterClass parent_class;
};

GtkTreeModel *gme_application_tree_model_filter_new (GtkTreeSelection *filter_selection);

#endif /* GME_APPLICATION_TREE_MODEL_FILTER_H */
